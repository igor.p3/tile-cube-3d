using com.adjust.sdk;
using UnityEngine;
using Matchmania;

namespace Moonee
{
    public class SDKStarter : MonoBehaviour
    {
        [SerializeField] private GameObject[] core;
        [SerializeField] private GDPR gdpr;
        [Header("SDKs to init")]


        [SerializeField] private Adjust adjust;

        private void Start()
        {
            if (GameController.isFirstSession == true)
            {
                DontDestroyOnLoad(this);
                gdpr.OnCompleted += OnGDPRCompleted;
                gdpr.gameObject.SetActive(true);
            }
            else
            {
                foreach (var item in core)
                {
                    item.SetActive(true);
                }
            }
        }

        private void OnGDPRCompleted()
        {
            InitAll();
        }

        private void InitAll()
        {
            // Init your SDK here
            AdjustInit();
            foreach (var item in core)
            {
                item.SetActive(true);
            }
            
            SendConsent();
        }

        private void SendConsent()
        {
            IronSource.Agent.setConsent(true);
            IronSource.Agent.setMetaData("do_not_sell", "false");
            IronSource.Agent.setMetaData("is_child_directed", "false");
        }

        private void AdjustInit()
        {
            var adjustConfig = new AdjustConfig(adjust.appToken, adjust.environment, (adjust.logLevel == AdjustLogLevel.Suppress));
            adjustConfig.setLogLevel(adjust.logLevel);
            adjustConfig.setSendInBackground(adjust.sendInBackground);
            adjustConfig.setEventBufferingEnabled(adjust.eventBuffering);
            adjustConfig.setLaunchDeferredDeeplink(adjust.launchDeferredDeeplink);
            Adjust.start(adjustConfig);

            AdjustThirdPartySharing adjustThirdPartySharing = new AdjustThirdPartySharing(true);
            Adjust.trackThirdPartySharing(adjustThirdPartySharing);
        }

//        private void IronsourceInit()
//        {
//            var developerSettings = Resources.Load<IronSourceMediationSettings>(IronSourceConstants.IRONSOURCE_MEDIATION_SETTING_NAME);
//            var appKey = "";
//            if (developerSettings != null)
//            {
//#if UNITY_ANDROID
//                appKey = developerSettings.AndroidAppKey;
//#elif UNITY_IOS
//            appKey = developerSettings.IOSAppKey;
//#endif
//            }
//            IronSource.Agent.init(appKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL, IronSourceAdUnits.OFFERWALL, IronSourceAdUnits.BANNER);

//            //InitializeRewardedAds();
//            //InitializeInterstitialAds();
//            //InitializeBannerAds();

//            // Integration helper to test if all configurated well
//            // IronSource.Agent.validateIntegration();

//            // For monitoring the internet connection on the user�s device through the ironSource Network Change Status function
//            // IronSource.Agent.shouldTrackNetworkState(true);
//        }
    }
}
