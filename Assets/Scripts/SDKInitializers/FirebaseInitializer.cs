﻿using UnityEngine;
using Firebase;
using Firebase.RemoteConfig;
using Firebase.Firestore;
using System.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;


namespace HyperCasualTemplate
{
    public class FirebaseInitializer : MonoBehaviour
    {
        public static FirebaseApp FirebaseApp { get; private set; }
        public static FirebaseRemoteConfig remoteConfig { get; private set; }

        public static Dictionary<string, object> defaults = new Dictionary<string, object>();

        private void Start()
        {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available)
                {
                    FirebaseApp = FirebaseApp.DefaultInstance;
                    remoteConfig = FirebaseRemoteConfig.DefaultInstance;

                    InitializeFirebaseConfig();
                }
                else
                {
                    Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                }
            });
        }

        void InitializeFirebaseConfig()
        {
            defaults.Add("StartLevel", 5);
            defaults.Add("LevelCooldown", 2);
            Debug.Log("Remote config ready!");
            FetchFireBase();
        }
        public void FetchFireBase()
        {
            FetchDataAsync();
        }
        public void ShowData()
        {
            Debug.Log("StartLevel: " + remoteConfig.GetValue("StartLevel").LongValue);
            Debug.Log("LevelCooldown: " + remoteConfig.GetValue("LevelCooldown").LongValue);

            defaults["StartLevel"] = remoteConfig.GetValue("StartLevel").LongValue;
            defaults["LevelCooldown"] = remoteConfig.GetValue("LevelCooldown").LongValue;

        }

        // Start a fetch request.
        public Task FetchDataAsync()
        {
            Debug.Log("Fetching data...");
            // FetchAsync only fetches new data if the current data is older than the provided
            // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
            // By default the timespan is 12 hours, and for production apps, this is a good
            // number.  For this example though, it's set to a timespan of zero, so that
            // changes in the console will always show up immediately.
            Task fetchTask = remoteConfig.FetchAsync(TimeSpan.Zero);
            return fetchTask.ContinueWith(FetchComplete);
        }

        void FetchComplete(Task fetchTask)
        {
            if (fetchTask.IsCanceled)
            {
                Debug.Log("Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                Debug.Log("Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                Debug.Log("Fetch completed successfully!");
            }

            var info = remoteConfig.Info;
            switch (info.LastFetchStatus)
            {
                case LastFetchStatus.Success:
                    remoteConfig.ActivateAsync();
                    Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).",
                        info.FetchTime));
                    ShowData();
                    break;
                case LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case FetchFailureReason.Error:
                            Debug.Log("Fetch failed for unknown reason");
                            break;
                        case FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                    }
                    break;
                case LastFetchStatus.Pending:
                    Debug.Log("Latest Fetch call still pending.");
                    break;
            }
        }
    }
}
