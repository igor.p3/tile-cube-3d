using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Firestore;
using Matchmania;
using UnityEngine;

public class TournamentModel
{
    public DocumentReference tourInfoDocument;
    public CollectionReference RoomCollectionReference; 
    public RealTimeTimer localTimer;
    public Dictionary<int, TTEventDataModel.Loot> LootTable = new Dictionary<int, TTEventDataModel.Loot>()
    {
        {0, new TTEventDataModel.Loot(500, TTEventDataModel.LootType.Coins)},
        {1, new TTEventDataModel.Loot(300, TTEventDataModel.LootType.Coins)},
        {2, new TTEventDataModel.Loot(200, TTEventDataModel.LootType.Coins)},
        {3, new TTEventDataModel.Loot(2, TTEventDataModel.LootType.Hint)},
        {4, new TTEventDataModel.Loot(2, TTEventDataModel.LootType.Hint)},
        {5, new TTEventDataModel.Loot(1, TTEventDataModel.LootType.Hint)},
        {6, new TTEventDataModel.Loot(1, TTEventDataModel.LootType.Hint)},
        {7, new TTEventDataModel.Loot(1, TTEventDataModel.LootType.Hint)},
        {8, new TTEventDataModel.Loot(1, TTEventDataModel.LootType.Hint)},
        {9, new TTEventDataModel.Loot(1, TTEventDataModel.LootType.Hint)},
        
    };
}

public enum TournamentType
{
    Level,
    Star,
}

[FirestoreData]
public class TournamentServerData
{
    [FirestoreProperty] public TournamentType tournamentType { get; set; }
    [FirestoreProperty] public long timeForEvent { get; set; }
    [FirestoreProperty] public string tournamentId { get; set; }
    [FirestoreProperty] public Timestamp timeStarted { get; set; }

    public TournamentServerData(TournamentType tournamentType = default, long timeForEvent = default, string tournamentId = default, Timestamp timeStarted = default)
    {
        this.tournamentType = tournamentType;
        this.timeForEvent = timeForEvent;
        this.tournamentId = tournamentId;
        this.timeStarted = timeStarted;
    }

    public TournamentServerData()
    {
        this.tournamentType = TournamentType.Level;
        timeForEvent = 0;
        tournamentId = "";
        timeStarted = new Timestamp();
    }
}

[FirestoreData]
public class TourLeaderboardPlayer
{
    [FirestoreProperty] public string playerName { get; set; }
    [FirestoreProperty] public long passedLevel { get; set; }
    [FirestoreProperty] public long collectedStars { get; set; }
    [FirestoreProperty] public string country { get; set; }

    public TourLeaderboardPlayer(string playerName = null, long passedLevel = default, long collectedStars = default, string country = null)
    {
        this.playerName = playerName;
        this.passedLevel = passedLevel;
        this.collectedStars = collectedStars;
        this.country = country;
    }

    public TourLeaderboardPlayer()
    {
        playerName = GameController.playerName;
        passedLevel = 0;
        collectedStars = 0;
        country = "Australia";
    }
}
