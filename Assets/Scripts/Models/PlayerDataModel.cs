using System.Collections.Generic;
using Firebase.Firestore;

namespace Matchmania
{
    [FirestoreData]
    public class PlayerDataModel
    {
        [FirestoreProperty] public string playerName { get; set; }
        [FirestoreProperty] public long passedLevel { get; set; }
        [FirestoreProperty] public string country { get; set; }

        public PlayerDataModel()
        {
            playerName = "";
            passedLevel = 0;
            country = "Australia";
        }

        public PlayerDataModel (string playerName, long passedLevel, string country)
        {
            this.playerName = playerName;
            this.passedLevel = passedLevel;
            this.country = country;
        }
    }
    [System.Serializable]
    public class PlayerDataModels
    {
        public PlayerDataModel[] playerDataModels;
    }
}