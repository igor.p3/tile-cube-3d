﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace Matchmania
{
    public class GameModel
    {
        //private static string levelsConfigURL = "https://script.google.com/macros/s/AKfycbwKcV5USO9MudSDUuSfyLEi_o2FXf3WA3MymsldG_zMtCnqCKM/exec";
        //private static string itemsProgressionURL = "https://script.google.com/macros/s/AKfycbzMA-BWVwebE1mRpmN-rNwCBgRlQgNyXsDXdJiYXeeyNVsFOeE/exec";
        //private static string timeHeistProgressionURL = "";
        //private static string diomandGains = "";

        public const int itemMatchCount = 3;

        public static LevelsConfig Levels;
        public static PacksProgressionConfig Packs;
        public static TimeHeistProgression TimeHeistProgressions;
        public static DiomandsGainModels DiomandsGains;


        public static int loadingIndex = 0;

        public static void LoadlevelConfig(MonoBehaviour component)
        {
            component.StartCoroutine(DownloadLevelsConfig());
        }
        public static void LoadItemsProgressionConfig(MonoBehaviour component)
        {
            component.StartCoroutine(DownloadItemsProgressionConfig());
        }
        public static void LoadTimeHeistProgressionConfig(MonoBehaviour component)
        {
            component.StartCoroutine(DownloadTimeHeistPregressionConfig());
        }
        public static void LoadDiomandsGainsProgressionConfig(MonoBehaviour component)
        {
            component.StartCoroutine(DownloadDiomandsGainsConfig());
        }
        private static IEnumerator DownloadDiomandsGainsConfig()
        {
            //UnityWebRequest www = UnityWebRequest.Get(levelsConfigURL);
            //yield return www.SendWebRequest();

            TextAsset textAsset = Resources.Load<TextAsset>("DiomandGains");
            string json = textAsset.text;

            //if (www.isNetworkError || www.isHttpError)
            //{
            //    Debug.Log(www.error);
            //}
            //else
            //{
            //    json = www.downloadHandler.text;
            //}
            DiomandsGains = JsonUtility.FromJson<DiomandsGainModels>(json);

            loadingIndex++;
            yield return null;
        }
        private static IEnumerator DownloadLevelsConfig()
        {
            //UnityWebRequest www = UnityWebRequest.Get(levelsConfigURL);
            //yield return www.SendWebRequest();

            TextAsset textAsset = Resources.Load<TextAsset>("LevelsConfig");
            string json = textAsset.text;

            //if (www.isNetworkError || www.isHttpError)
            //{
            //    Debug.Log(www.error);
            //}
            //else
            //{
            //    json = www.downloadHandler.text;
            //}
            Levels = JsonUtility.FromJson<LevelsConfig>(json);

            loadingIndex++;
            yield return null;
        }
        private static IEnumerator DownloadTimeHeistPregressionConfig()
        {
            //UnityWebRequest www = UnityWebRequest.Get(levelsConfigURL);
            //yield return www.SendWebRequest();

            TextAsset textAsset = Resources.Load<TextAsset>("TimeHeistPregressionConfig");
            string json = textAsset.text;

            //if (www.isNetworkError || www.isHttpError)
            //{
            //    Debug.Log(www.error);
            //}
            //else
            //{
            //    json = www.downloadHandler.text;
            //}
            TimeHeistProgressions = JsonUtility.FromJson<TimeHeistProgression>(json);

            loadingIndex++;
            yield return null;
        }
        private static IEnumerator DownloadItemsProgressionConfig()
        {
            //UnityWebRequest www = UnityWebRequest.Get(itemsProgressionURL);
            //yield return www.SendWebRequest();

            TextAsset textAsset = Resources.Load<TextAsset>("PacksProgressionConfig");
            string json = textAsset.text;

            //if (www.isNetworkError || www.isHttpError)
            //{
            //    Debug.Log(www.error);
            //}
            //else
            //{
            //    json = www.downloadHandler.text;
            //}
            Packs = JsonUtility.FromJson<PacksProgressionConfig>(json);

            loadingIndex++;
            yield return null;
        }
    }
    [System.Serializable]
    public class LevelsConfig
    {
      public  LevelModel[] Levels;
    }
    [System.Serializable]
    public class PacksProgressionConfig
    {
        public PacksProgressionModel[] Packs;
    }
    [System.Serializable]
    public class TimeHeistProgression
    {
        public TimeHeistModel[] TimeHeistProgressions;
    }
}