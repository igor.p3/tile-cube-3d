using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Matchmania
{
    [System.Serializable]
    public class TimeHeistModel
    {
        public int level;
        public int diamondsCount;
        public string rewardType;
        public int rewardCount;
    }
}