﻿
using System.Collections.Generic;

namespace Matchmania
{
    [System.Serializable]
    public class LevelProgressDataModel
    {
        public List<ItemDataModel> itemsData = new List<ItemDataModel>();

        public float timer;
        public float levelsStars;

        public int CheckSlots()
        {
            int count = 0;

            foreach (var item in itemsData)
            {
                if(item.slotIndex != -1)
                {
                    count++;
                }
            }

            return count;
        }
    }

    [System.Serializable]
    public class ItemDataModel
    {
        public string name;
        public float positionX;
        public float positionY;
        public float positionZ;
        public int slotIndex = -1;
    }
}