﻿using System.Collections.Generic;

namespace Matchmania
{
    [System.Serializable]
    public class LevelModel
    {
        public float timer;
        public string iconsPack;
        public int shape;
        public int iconsCount;
    }
    public enum IconsPackType
    {
        animals,
        sweets,
        fruits,
        food,
        toys,
        animals_toys_sweets_food_drinks_plants
    }
}
