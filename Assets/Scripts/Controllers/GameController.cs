﻿using UnityEngine;
using HyperCasualTemplate;
using System.Collections;
using Firebase.Firestore;
using System.Collections.Generic;
using System;

namespace Matchmania
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private Sprite[] fruits;
        [SerializeField] private Sprite[] sweets;
        [SerializeField] private Sprite[] food;
        [SerializeField] private Sprite[] animals;
        [SerializeField] private Sprite[] toys;
        [SerializeField] private Sprite[] animals_toys_sweets_food_drinks_plants;
        [SerializeField] private Sprite[] test;

        public static Sprite[] fruitsIcons { get; private set; }
        public static Sprite[] sweetsIcons { get; private set; }
        public static Sprite[] foodIcons { get; private set; }
        public static Sprite[] animalsIcons { get; private set; }
        public static Sprite[] toysIcons { get; private set; }
        public static Sprite[] animals_toys_sweets_food_drinks_plantsIcons { get; private set; }
        public static Sprite[] testIcons { get; private set; }

        public static SaveDataModel SavedData { get; private set; }

        public static bool isFirstSession = true;
        public static bool isTookFirstDailyReward = false;

        public static string playerName;
        public static int passedLevels;

        void Awake()
        {
            SavedData = LoadGameData();
            Physics.autoSyncTransforms = false;
        }
        private void Start()
        {
            Application.targetFrameRate = 300;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            fruitsIcons = fruits;
            sweetsIcons = sweets;
            foodIcons = food;
            animalsIcons = animals;
            toysIcons = toys;
            animals_toys_sweets_food_drinks_plantsIcons = animals_toys_sweets_food_drinks_plants;
            testIcons = test;

        }
        public static void SaveGameData()
        {
            SaveDataModel saveGameData = new SaveDataModel();

            saveGameData.starsCount = CurrencyController.Stars;
            saveGameData.coinsCount = CurrencyController.Coins;
            saveGameData.bestScore = FakeLeaderboardController.bestScore;
            saveGameData.playerPlace = FakeLeaderboardController.playerPlace;
            saveGameData.fakeLeaderboardStars = CurrencyController.FakeLeaderboardStars;
            saveGameData.savedDateTime = ReviveView.savedDateTime;

            // Hearts
            saveGameData.heartsCount = CurrencyController.Hearts;
            saveGameData.isUnlimitedHearts = HeartsController.isUnlimited;
            saveGameData.unlimTimeHearts = HeartsController.timeForUnlim;
            saveGameData.heartsDateTime = HeartsController.HeartsStartTime.ToString();

            saveGameData.hintsCount = CurrencyController.Hints;
            saveGameData.fansCount = CurrencyController.Fans;
            saveGameData.starsChestCount = CurrencyController.StarsChestCount;
            saveGameData.levelsChestCount = CurrencyController.LevelsChestCount;
            saveGameData.levelIndex = LevelController.CurrentLevelIndex;
            saveGameData.isFirstSession = isFirstSession;
            // Special offer
            saveGameData.savedCurrentOfferType = SpecialOfferController.SavedCurrentOfferType;
            saveGameData.savedOfferStartTime = SpecialOfferController.OfferStartTime.ToString();
            saveGameData.isSpecialOfferHidden = SpecialOfferController.IsOfferHidden;
            // Region related
            saveGameData.countryDisplayName = RegionIdentifier.savedCountryName;
            saveGameData.isRegionFetchedByIp = RegionIdentifier.isRegionFetchedByIP;

            saveGameData.isNicknameChanged = ChangeNicknameView.IsNicknameChanged;
            saveGameData.isCanShowRateUs = RateUsView.isCanShowRateUs;
            saveGameData.isMusicOn = SettingsController.isMusicOn;
            saveGameData.isVibroOn = SettingsController.isVibrationOn;
            saveGameData.isSoundOn = SettingsController.isSoundOn;
            saveGameData.stringLastRewardDate = DailyBonusController.stringLastRewardDate;
            saveGameData.playerName = playerName;
            saveGameData.passedLevels = passedLevels;
            saveGameData.isStarterBundlePurchased = IAPController.isStarterBundlePurchased;
            saveGameData.isTookFirstDailyReward = isTookFirstDailyReward;
            saveGameData.isAdsRemoved = AdvertisementSystem.isAdsRemoved;
            saveGameData.timeHeistDiamondsToAdd = CurrencyController.TimeHeistDiamondsToAdd;
            saveGameData.starsChestCountToAdd = CurrencyController.StarsChestCountToAdd;

            SavingSystem<SaveDataModel>.SaveJsonGameData(saveGameData);
        }
        public static void OnDataChangedLoad()
        {
            SavedData = LoadGameData();
        }
        public static SaveDataModel LoadGameData(SaveDataModel optionalDataModel = null)
        {
            var saveGameData = optionalDataModel ?? SavingSystem<SaveDataModel>.LoadJsonGameData();

            if (saveGameData != null)
            {
                CurrencyController.Stars = saveGameData.starsCount;
                CurrencyController.Coins = saveGameData.coinsCount;
                CurrencyController.Hints = saveGameData.hintsCount;
                CurrencyController.Fans = saveGameData.fansCount;
                FakeLeaderboardController.bestScore = saveGameData.bestScore;
                FakeLeaderboardController.playerPlace = saveGameData.playerPlace;
                CurrencyController.FakeLeaderboardStars = saveGameData.fakeLeaderboardStars;
                ReviveView.savedDateTime = saveGameData.savedDateTime;
                CurrencyController.TimeHeistDiamonds = saveGameData.timeHeistDiamonds;
                TimeHeistController.stringTimeHeistDateTime = saveGameData.timeHeistDateTime;

                // Hearts
                HeartsController.isUnlimited = saveGameData.isUnlimitedHearts;
                HeartsController.timeForUnlim = saveGameData.unlimTimeHearts;
                HeartsController.HeartsSavedStartTime = saveGameData.heartsDateTime;
                CurrencyController.Hearts = saveGameData.heartsCount;

                CurrencyController.LevelsChestCount = saveGameData.levelsChestCount;
                CurrencyController.StarsChestCount = saveGameData.starsChestCount;
                LevelController.CurrentLevelIndex = saveGameData.levelIndex;

                // Settings
                SettingsController.isMusicOn = saveGameData.isMusicOn;
                SettingsController.isSoundOn = saveGameData.isSoundOn;
                SettingsController.isVibrationOn = saveGameData.isVibroOn;
                isFirstSession = saveGameData.isFirstSession;
                // Special offer
                SpecialOfferController.SavedCurrentOfferType = saveGameData.savedCurrentOfferType;
                SpecialOfferController.SavedOfferStartTime = saveGameData.savedOfferStartTime;
                SpecialOfferController.IsOfferHidden = saveGameData.isSpecialOfferHidden;
                // Region related
                RegionIdentifier.isRegionFetchedByIP = saveGameData.isRegionFetchedByIp;
                RegionIdentifier.savedCountryName = saveGameData.countryDisplayName;

                ChangeNicknameView.IsNicknameChanged = saveGameData.isNicknameChanged;
                RateUsView.isCanShowRateUs = saveGameData.isCanShowRateUs;
                AdvertisementSystem.isAdsRemoved = saveGameData.isAdsRemoved;
                DailyBonusController.stringLastRewardDate = saveGameData.stringLastRewardDate;
                playerName = saveGameData.playerName;
                passedLevels = saveGameData.passedLevels;
                IAPController.isStarterBundlePurchased = saveGameData.isStarterBundlePurchased;
                isTookFirstDailyReward = saveGameData.isTookFirstDailyReward;
                CurrencyController.TimeHeistDiamondsToAdd = saveGameData.timeHeistDiamondsToAdd;
                CurrencyController.StarsChestCountToAdd = saveGameData.starsChestCountToAdd;

            }
            return saveGameData;
        }
    }
}