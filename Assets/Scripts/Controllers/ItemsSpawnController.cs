﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using System.Linq;
using Lean.Pool;
using TileCube3D;
using System;

namespace Matchmania
{
    public class ItemsSpawnController : MonoBehaviour
    {
        #region Singleton
        private static ItemsSpawnController _instance;
        public static ItemsSpawnController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<ItemsSpawnController>();
                }

                return _instance;
            }
        }
        #endregion

        [SerializeField] private SFXConfig SFXConfig;

        public LevelShapeController currentShapeController;

        [SerializeField] private LevelShapeController[] shapeController;
        [SerializeField] private LevelShapeController tutorialShapeController;
        [SerializeField] private ItemController itemController;

        private bool isSpawningItems;
        private float spawnOffset = 25;
        public void SpawnItems()
        {
            if (isSpawningItems == false)
            {
                StartCoroutine(_SpawnItems());
            }
        }
        public IEnumerator _SpawnItems()
        {
            isSpawningItems = true;

            if(LevelController.CurrentLevelIndex == 0)
            {
                currentShapeController = Instantiate(tutorialShapeController);
            }
            else currentShapeController = Instantiate(shapeController[LevelController.Instance.CurrentLevelModel.shape]);

            LevelController.Instance.IsCanMakeTurn = false;
            LevelController.Instance.IsPlayingLevel = false;

            int count = currentShapeController.positions.Count / 3;

            for (int i = 0; i < count; i++)
            {
                //Sprite sprite = GetLevelIcons()[UnityEngine.Random.Range(0, GetLevelIcons().Count)];
                Sprite sprite =GameController.testIcons[UnityEngine.Random.Range(0, GameController.testIcons.Length)];

                for (int j = 0; j < 3; j++)
                {
                    ItemController item = LeanPool.Spawn(itemController);
                    item.SetItem(sprite.name, sprite);
                    Transform position = currentShapeController.positions[UnityEngine.Random.Range(0, currentShapeController.positions.Count)];
                    item.transform.position = position.position;
                    item.transform.localPosition += Vector3.up * spawnOffset;
                    item.transform.rotation = position.rotation;
                    item.isRotate = false;
                    item.GetComponent<Collider>().enabled = true;
                    item.startTransformPosition = position;
                    item.gameObject.SetActive(true);
                    LevelController.Instance.CurrentLevelItems.Add(item);
                    item.transform.SetParent(position);
                    item.transform.localScale = position.localScale;
                    item.DeselectItem();
                    currentShapeController.positions.Remove(position);
                }
            }

            yield return null;

            List<ItemController[]> items = new List<ItemController[]>();

            foreach (var layer in currentShapeController.layers)
            {
                ItemController[] _items = layer.GetComponentsInChildren<ItemController>();
                items.Add(_items);
            }

            for (int i = 0; i < items.Count; i++)
            {
                for (int j = 0; j < items[i].Length; j++)
                {
                    var move = items[i][j].transform.DOLocalMove(Vector3.zero, 0.1f).OnComplete (() => { items[i][j].dustParticle.Play(); });
                    move.SetEase(Ease.OutCubic);
                    yield return new WaitForSeconds(0.025f);
                }
            }

            LevelController.Instance.IsCanMakeTurn = true;
            LevelController.Instance.IsPlayingLevel = true;

            isSpawningItems = false;
        }
        public void PlayHoseSoundEffect()
        {
            SFXConfig.PlaySoundEffect(SFXConfig.hose);
        }
        public void DespawnItems()
        {
            for (int i = 0; i < LevelController.Instance.CurrentLevelItems.Count; i++)
            {
                LeanPool.Despawn(LevelController.Instance.CurrentLevelItems[i].gameObject);
            }
        }
        public static List<Sprite> GetLevelIcons()
        {
            List<Sprite> levelIcons = new List<Sprite>();

            if(LevelController.Instance.CurrentLevelModel.iconsPack == IconsPackType.animals.ToString())
            {
                levelIcons = GameController.animalsIcons.ToList().OrderBy(n => Guid.NewGuid()).Take(LevelController.Instance.CurrentLevelModel.iconsCount).ToList();
            }
            else if (LevelController.Instance.CurrentLevelModel.iconsPack == IconsPackType.food.ToString())
            {
                levelIcons = GameController.foodIcons.ToList().OrderBy(n => Guid.NewGuid()).Take(LevelController.Instance.CurrentLevelModel.iconsCount).ToList();
            }
            else if (LevelController.Instance.CurrentLevelModel.iconsPack == IconsPackType.fruits.ToString())
            {
                levelIcons = GameController.fruitsIcons.ToList().OrderBy(n => Guid.NewGuid()).Take(LevelController.Instance.CurrentLevelModel.iconsCount).ToList();
            }
            else if (LevelController.Instance.CurrentLevelModel.iconsPack == IconsPackType.sweets.ToString())
            {
                levelIcons = GameController.sweetsIcons.ToList().OrderBy(n => Guid.NewGuid()).Take(LevelController.Instance.CurrentLevelModel.iconsCount).ToList();
            }
            else if (LevelController.Instance.CurrentLevelModel.iconsPack == IconsPackType.toys.ToString())
            {
                levelIcons = GameController.toysIcons.ToList().OrderBy(n => Guid.NewGuid()).Take(LevelController.Instance.CurrentLevelModel.iconsCount).ToList();
            }
            else if (LevelController.Instance.CurrentLevelModel.iconsPack == IconsPackType.animals_toys_sweets_food_drinks_plants.ToString())
            {
                levelIcons = GameController.animals_toys_sweets_food_drinks_plantsIcons.ToList().OrderBy(n => Guid.NewGuid()).Take(LevelController.Instance.CurrentLevelModel.iconsCount).ToList();
            }
            return levelIcons;
        }
    }
}