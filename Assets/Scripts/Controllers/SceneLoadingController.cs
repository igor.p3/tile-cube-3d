﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

namespace Matchmania
{
    public class SceneLoadingController : MonoBehaviour
    {
        [SerializeField] private Slider loadingSlider;

        void Start()
        {
            GameModel.LoadlevelConfig(this);
            GameModel.LoadItemsProgressionConfig(this);
            GameModel.LoadTimeHeistProgressionConfig(this);
            GameModel.LoadDiomandsGainsProgressionConfig(this);

            StartCoroutine(LoadScene());
        }
        public IEnumerator LoadScene()
        {
            float perscents = loadingSlider.maxValue - loadingSlider.value;

            for (int i = 0; i < perscents; i++)
            {
                loadingSlider.value++;
                yield return null;
            }
            SceneManager.LoadScene(1);
        }
    }
}