using Firebase.Firestore;

public static class DatabaseModel
{
    private static FirebaseFirestore firestoreDatabase;
    public static FirebaseFirestore FirestoreDatabase
    {
        get
        {
            if (firestoreDatabase == null)
            {
                firestoreDatabase = FirebaseFirestore.DefaultInstance;
            }

            return firestoreDatabase;
        }
    }
}
