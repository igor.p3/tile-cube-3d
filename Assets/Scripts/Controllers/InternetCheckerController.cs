using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public class InternetCheckerController : MonoBehaviour
{
    #region Singleton
    private static HeartsController _instance;
    public static HeartsController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<HeartsController>();
            }

            return _instance;
        }
    }
    #endregion
    
    public static bool isNoInternetConnection;
    public static event Action OnNoInternet; 
    
    private void Start()
    {
        OnNoInternet += () => isNoInternetConnection = true;
        OnNoInternet += () => Debug.LogError("There is no internet connection");
        
        CheckInternetConnection(() => isNoInternetConnection = false, OnNoInternet);
        
    }

    public void CheckInternetConnection(Action successCallback = null, Action noInternetCallback = null)
    {
#if !UNITY_EDITOR
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            noInternetCallback?.Invoke();
            return;
        }
#endif

        StartCoroutine(CheckInternetRoutine(successCallback, noInternetCallback));
    }
    
    private IEnumerator CheckInternetRoutine(Action successCallback = null, Action noInternetCallback = null)
    {
        UnityWebRequest webRequest = UnityWebRequest.Get("http://google.com");
        yield return webRequest.SendWebRequest();
        if (webRequest.result != UnityWebRequest.Result.Success || webRequest.result == UnityWebRequest.Result.ConnectionError)
        {
            noInternetCallback?.Invoke();
            yield break;
        }
        
        successCallback?.Invoke();
    }
}
