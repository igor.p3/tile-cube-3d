using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using HyperCasualTemplate;

namespace Matchmania
{
    public class FanController : MonoBehaviour
    {
        #region Singleton
        private static FanController _instance;
        public static FanController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<FanController>();
                }

                return _instance;
            }
        }
        #endregion

        public Action FanUsedEventHandler;

        [SerializeField] private SFXConfig SFXConfig;

        private bool isCanUseFan = true;

        public void UseFan()
        {
            if (LevelController.Instance.slots[0].ItemController == null) return;

            if (CurrencyController.Fans > 0)
            {
                StartCoroutine(_UseFan());
            }
            else
            {
                if(AdvertisementSystem.IsRewardedAdReady())
                {
                    AdvertisementSystem.ShowRewardedAd();
                    CurrencyController.Fans++;
                   
                }
            }
            HintController.Instance.CheckBoostersIcons();
        }
        public void Undo()
        {
            for (int i = LevelController.Instance.slots.Length - 1; i >= 0; i--)
            {
                if (LevelController.Instance.slots[i].ItemController != null)
                {
                    LevelController.Instance.slots[i].ItemController.BackToLevel();
                    LevelController.Instance.slots[i].ItemController = null;
                    break;
                }
            }
        }

        private IEnumerator _UseFan()
        {
            isCanUseFan = false;
            CurrencyController.Fans--;
            FanUsedEventHandler?.Invoke();

            Undo();

            GameController.SaveGameData();
            yield return null;
            isCanUseFan = true;
        }
    }
}