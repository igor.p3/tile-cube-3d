﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using MoreMountains.NiceVibrations;

namespace Matchmania
{
    public class ItemController : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer[] spriteRenderes;
        public ParticleSystem dustParticle;

        public Color selectedColor;
        public Color unselectedColor;

        const float movementDuration = 0.2f;
        public bool isRotate { get; set; }

        public Material material;

        private static float yRotation;
        private static float yRotationSpeed = 10f;

        public Transform startTransformPosition;

        private void Awake()
        {
            material = GetComponent<MeshRenderer>().material;
        }
        public void SetItem(string name, Sprite sprite)
        {
            gameObject.name = name;

            foreach (var spriteRenderer in spriteRenderes)
            {
                spriteRenderer.sprite = sprite;
            }
        }
        public IEnumerator MoveToTarget(Vector3 targetPosition, Action OnComplete = null)
        {
            Vector3 startPosition = transform.position;
            Quaternion startRoation = transform.rotation;
            Quaternion endRotation = Quaternion.Euler(new Vector3(20, transform.rotation.eulerAngles.y, 0));
            transform.DORotate(endRotation.eulerAngles, 0.5f);
            isRotate = true;
            var move = transform.DOMove(targetPosition + Vector3.up, movementDuration);
            move.SetEase(Ease.Unset);
            yield return new WaitForSeconds(movementDuration);
            OnComplete?.Invoke();
            Debug.Log(gameObject.name);
        }
        private void Update()
        {
            if (isRotate == true)
            {
                transform.localRotation = Quaternion.Euler(20, 0, yRotation);
                yRotation += Time.deltaTime * yRotationSpeed;
            }
            if (LevelController.Instance.IsCanMakeTurn == false)
            {
                DeselectItem();
            }
        }
        public void BackToLevel()
        {

            var move = transform.DOMove(startTransformPosition.position, movementDuration);
            var rotate = transform.DORotate(startTransformPosition.rotation.eulerAngles, movementDuration);
            var scale = transform.DOScale(startTransformPosition.localScale, movementDuration);
            move.SetEase(Ease.OutCubic);
            rotate.SetEase(Ease.OutCubic);
            scale.SetEase(Ease.OutCubic);
            transform.SetParent(startTransformPosition);
            isRotate = false;
        }
        public void SetSelectedItem(int touchIndex)
        {
            if (LevelController.Instance.IsCanMakeTurn && LevelController.Instance.IsPlayingLevel)
            {
                for (int i = 0; i < PlayerController.selectedItems[touchIndex].Count; i++)
                {
                    PlayerController.selectedItems[touchIndex][i].DeselectItem();
                    PlayerController.selectedItems[touchIndex].Clear();
                }

                //if (SettingsController.isVibrationOn == true) MMVibrationManager.Haptic(HapticTypes.SoftImpact, false, true, this);

                PlayerController.selectedItems[touchIndex].Add(this);
                material.DOColor(selectedColor, 0.25f);
                LevelController.isPausedComboTimer = false;
            }
        }
        public void DeselectItem()
        {
            material.DOColor(unselectedColor, 0.25f);
        }
    }
}