using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Matchmania
{
    public class FakeLeaderboardController : MonoBehaviour
    {
        public static Action OpenAtLevelEndEventHandler;

        public static int bestScore;
        public static int playerPlace = 15000;

        private int maxScore = 49965;
        private int minScore = 1;

        public static int GetBestScore(int currentLevelScore)
        {
            int score = bestScore;

            if (currentLevelScore > bestScore)
            {
                score = currentLevelScore;
                bestScore = score;
                GameController.SaveGameData();
            }
            return score;
        }
        public static string NameGenerator()
        {
            string name = "";

            string[] nameComponent1 = new string[] { "Ge", "Me", "Ta", "Bo", "Ke", "Ra", "Ne", "Mi" };
            string[] nameComponent2 = new string[] { "oo", "ue", "as", "to", "ra", "me", "io", "so" };
            string[] nameComponent3 = new string[] { "sen", "matt", "lace", "fo", "cake", "end" };

            string nameCompfirst = nameComponent1[Random.Range(0, nameComponent1.Length)];
            string nameCompSecond = nameComponent2[Random.Range(0, nameComponent2.Length)];
            string nameCompThird = nameComponent3[Random.Range(0, nameComponent3.Length)];

            return name = nameCompfirst + nameCompSecond + nameCompThird;
        }
        public static Sprite GenerateCountry()
        {
            int rnd = Random.Range(0, LeaderBoardView.flags.Length);
            return LeaderBoardView.flags[rnd];
        }
    }
}