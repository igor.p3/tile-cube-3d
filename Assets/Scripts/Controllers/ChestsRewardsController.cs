using System.Collections;
using Matchmania;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChestsRewardsController : MonoBehaviour
{
    #region Singleton
    private static ChestsRewardsController _instance;
    public static ChestsRewardsController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ChestsRewardsController>();
            }

            return _instance;
        }
    }
    #endregion

    [SerializeField] private MainMenuView MainMenuView;
    [SerializeField] private float unlim15MINBooster = 60, unlim10MINBooster = 30;

    public int coinsCount;
    public void GenerateRewards(Button claimButton, GameObject chestScreen, Animator chestAnimator, Animator rewardAnimator, Image chestRewardsImage, MainMenuView mainMenuView)
    {
        var random = Random.Range(0, 3);

        switch (random)
        {
            //case 0:
            //{
            //    //Give hint
            //    int rndHints = Random.Range(1, 3);
            //    StartCoroutine(OpenChest(claimButton, chestScreen, chestAnimator, rewardAnimator, chestRewardsImage, mainMenuView.hintSprite,
            //        rndHints.ToString()));
            //    chestRewardsImage.rectTransform.sizeDelta = mainMenuView.hintSprite.rect.size;
            //    CurrencyController.Hints += rndHints;
            //    break;
            //}
            //case 1:
            //{
            //    //Give fans
            //    int rndFans = Random.Range(1, 3);
            //    StartCoroutine(
            //        OpenChest(claimButton, chestScreen, chestAnimator, rewardAnimator, chestRewardsImage, mainMenuView.fanSprite, rndFans.ToString()));
            //    chestRewardsImage.rectTransform.sizeDelta = mainMenuView.fanSprite.rect.size;
            //    CurrencyController.Fans += rndFans;
            //    break;
            //}
            //case 2:
            //{
            //    // Give Unlim Hearts
            //    var rndHearts = Random.Range(0, 100);

            //    if (rndHearts <= unlim15MINBooster)
            //    {
            //        // Give 15 min unlim
            //        StartCoroutine(OpenChest(chestScreen, chestAnimator, chestRewardsImage,
            //            mainMenuView.unlimHeartsSprite, "15"));
            //        chestRewardsImage.rectTransform.sizeDelta = mainMenuView.unlimHeartsSprite.rect.size;
            //        HeartsController.Instance.SwitchToUnlimited(900);
            //        break;
            //    }

            //    if (rndHearts <= unlim10MINBooster)
            //    {
            //        // Give 10 min unlim
            //        StartCoroutine(OpenChest(chestScreen, chestAnimator, chestRewardsImage,
            //            mainMenuView.unlimHeartsSprite, "10"));
            //        chestRewardsImage.rectTransform.sizeDelta = mainMenuView.unlimHeartsSprite.rect.size;
            //        HeartsController.Instance.SwitchToUnlimited(600);
            //        break;
            //    }

            //    // Give 5 min unlim
            //    StartCoroutine(OpenChest(chestScreen, chestAnimator, chestRewardsImage,
            //        mainMenuView.unlimHeartsSprite, "5"));
            //    chestRewardsImage.rectTransform.sizeDelta = mainMenuView.unlimHeartsSprite.rect.size;
            //    HeartsController.Instance.SwitchToUnlimited(300);
            //    break;
            //}
            default:
                {
                    //Give coins
                    //var rndCoins = Random.Range(0, 3);
                    int coinsToAdd = Random.Range(100, 500);
                    //if (rndCoins == 0)
                    //{
                    //    coinsToAdd = 50;
                    //}
                    //else if (rndCoins == 1)
                    //{
                    //    coinsToAdd = 100;
                    //}
                    //else coinsToAdd = 150;

                    StartCoroutine(OpenChest(claimButton, chestScreen, chestAnimator, rewardAnimator, chestRewardsImage, mainMenuView.coinsSprite,
                        coinsToAdd));
                    chestRewardsImage.rectTransform.sizeDelta = mainMenuView.coinsSprite.rect.size;
                    //CurrencyController.Coins += coinsToAdd;
                    coinsCount = coinsToAdd;
                    break;
                }
        }
    }
    private IEnumerator OpenChest(Button claimButton, GameObject chestScreen, Animator chestAnimator, Animator rewardAnimator, Image rewardImage, Sprite icon, int count)
    {

        //LevelView.OpenScreen(chestScreen);
        chestAnimator.Play("Opening");
        chestAnimator.enabled = true;
        yield return new WaitForSeconds(2.25f);



        rewardImage.gameObject.SetActive(true);
        rewardImage.sprite = icon;
        rewardAnimator.Play("Start");
        rewardImage.GetComponentInChildren<TextMeshProUGUI>().text = count.ToString();
        claimButton.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        chestAnimator.enabled = false;

        coinsCount = count;
    }

    public void OpenDailyChest(GameObject chestsScreen, GameObject rewardScreen, Image rewardImage,
        MainMenuView mainMenuView, TextMeshProUGUI rewardCountText)
    {
        StartCoroutine(ChestLogic(chestsScreen, rewardScreen, rewardImage, mainMenuView, rewardCountText));
    }

    private IEnumerator ChestLogic(GameObject chestsScreen, GameObject rewardScreen, Image rewardImage,
        MainMenuView mainMenuView, TextMeshProUGUI rewardCountText)
    {
        yield return new WaitForSeconds(1f);

        chestsScreen.SetActive(false);
        rewardScreen.SetActive(true);

        var random = Random.Range(0, 4);

        switch (random)
        {
            //case 0:
            //{
            //    //Give hint
            //    int rndHints = /*Random.Range(1, 3)*/ 2;
            //    rewardImage.sprite = mainMenuView.hintSprite;
            //    rewardImage.rectTransform.sizeDelta = mainMenuView.hintSprite.rect.size;
            //    rewardCountText.text = rndHints.ToString();
            //    CurrencyController.Hints += rndHints;
            //    break;
            //}
            //case 1:
            //{
            //    //Give fans
            //    int rndFans = /*Random.Range(1, 3)*/  2;
            //    rewardImage.sprite = mainMenuView.fanSprite;
            //    rewardImage.rectTransform.sizeDelta = mainMenuView.fanSprite.rect.size;
            //    rewardCountText.text = rndFans.ToString();
            //    CurrencyController.Fans += rndFans;
            //    break;
            //}

            //case 2:
            //{
            //    // Give Unlim Hearts
            //    var rndHearts = Random.Range(0, 100);

            //    if (rndHearts <= unlim15MINBooster)
            //    {
            //        // Give 15 min unlim
            //        rewardCountText.text = "15";
            //        HeartsController.Instance.SwitchToUnlimited(900);
            //    }

            //    else if (rndHearts <= unlim10MINBooster)
            //    {
            //        // Give 10 min unlim
            //        rewardCountText.text = "10";
            //        HeartsController.Instance.SwitchToUnlimited(600);
            //    }

            //    else
            //    {
            //        // Give 5 min unlim
            //        rewardCountText.text = "5";
            //        HeartsController.Instance.SwitchToUnlimited(300);
            //    }
            //    rewardImage.sprite = mainMenuView.unlimHeartsSprite;
            //    rewardImage.rectTransform.sizeDelta = mainMenuView.unlimHeartsSprite.rect.size;
            //    break;
            //}
            default:
                {
                    //Give coins
                    //int rndCoins = Random.Range(0, 4);
                    int coinsToAdd = Random.Range(100, 500);
                    //if (rndCoins == 0)
                    //{
                    //    coinsToAdd = 50;
                    //}
                    //else if (rndCoins == 1)
                    //{
                    //    coinsToAdd = 100;
                    //}
                    //else if (rndCoins == 2)
                    //{
                    //    coinsToAdd = 150;
                    //}
                    //else coinsToAdd = 200;

                    rewardImage.sprite = mainMenuView.coinsSprite;
                    rewardImage.rectTransform.sizeDelta = mainMenuView.coinsSprite.rect.size;
                    rewardCountText.text = coinsToAdd.ToString();
                    //CurrencyController.Coins += coinsToAdd;

                    coinsCount = coinsToAdd;
                    break;
                }
        }
        //mainMenuView.UpdateMainMenuView();
        GameController.SaveGameData();
    }


}
