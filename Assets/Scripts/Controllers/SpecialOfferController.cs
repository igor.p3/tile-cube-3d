using System;
using System.Collections;
using Matchmania;
using UnityEngine;

public class SpecialOfferController : MonoBehaviour
{
    #region Singleton
    private static SpecialOfferController _instance;
    public static SpecialOfferController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SpecialOfferController>();
            }

            return _instance;
        }
    }
    #endregion

    [SerializeField] private SpecialOfferView specialOfferView;
    public bool isSpecialOfferOpened;

    // Saved values
    public static SpecialOfferType SavedCurrentOfferType;
    public static bool IsOfferHidden;
    
    // Time related
    private int timeForSpecialOfferCycle = 172800;
    public static string SavedOfferStartTime;
    public static DateTime OfferStartTime;
    private Timer _localTimer;


    private void Start()
    {
        CheckSpecialOffer();
        specialOfferView.InitializeSpecialOfferButton();
    }
    
    private void RealTimeCheck()
    {
        DateTime.TryParse(SavedOfferStartTime, out OfferStartTime);
        var realTimer = new RealTimeTimer(OfferStartTime, timeForSpecialOfferCycle);
        
        if (realTimer.IsDone)
        {
            // Switch to next offer
            HideOfferScreen();
            SwitchToNextOffer();
            _localTimer = new Timer(172800);
            return;
        }
        
        _localTimer = new Timer(realTimer.TimeLeft);
    }

    private void SwitchToNextOffer()
    {
        switch (SavedCurrentOfferType)
        {
            case SpecialOfferType.Special:
                SavedCurrentOfferType = SpecialOfferType.Casual;
                break;
            case SpecialOfferType.Casual:
                SavedCurrentOfferType = SpecialOfferType.Exclusive;
                break;
            case SpecialOfferType.Exclusive:
                SavedCurrentOfferType = SpecialOfferType.Special;
                break;
        }
        
        OfferStartTime = DateTime.Now;
        ShowOfferScreen();
        IsOfferHidden = false;
        specialOfferView.ShowSpecialOfferButtons();
        _localTimer = new Timer(timeForSpecialOfferCycle);
        GameController.SaveGameData();
    }

    public void CheckSpecialOffer()
    {
        if (GameController.isFirstSession || IsOfferHidden)
        {
            specialOfferView.HideSpecialOfferButtons();
            specialOfferView.HideOfferScreen(false);
            return;
        }

        RealTimeCheck();
        
        ShowOfferScreen();
        specialOfferView.ShowSpecialOfferButtons();
        GameController.SaveGameData();
    }

    public void HideOfferScreen()
    {
        specialOfferView.HideOfferScreen(true);
        isSpecialOfferOpened = false;
    }

    public void ShowOfferScreen()
    {
        if (GameController.isFirstSession == false)
        {
            specialOfferView.ShowOfferScreen(SavedCurrentOfferType);
            isSpecialOfferOpened = true;
        }
    }

    public void HideOfferBought()
    {
        IsOfferHidden = true;
        StartCoroutine(HideTheOfferScreenAfter2Seconds());
        specialOfferView.HideSpecialOfferButtons();
        GameController.SaveGameData();
    }

    public IEnumerator HideTheOfferScreenAfter2Seconds()
    {
        yield return null;
        HideOfferScreen();
    }
    private void FixedUpdate()
    {
        if (_localTimer.IsDone)
        {
            SwitchToNextOffer();
            return;
        }
        var timeSpan = TimeSpan.FromSeconds(_localTimer.TimeLeft);
        specialOfferView.UpdateTimersText($"{(int)timeSpan.TotalHours}{timeSpan.ToString(@"\:mm\:ss")}");
    }
    private void OnApplicationFocus(bool focus)
    {
        DateTime focusOffTime = new DateTime();
        DateTime focusOnTime = new DateTime();

        if (focus == false)
        {
            focusOffTime = DateTime.Now;
        }
        else
        {
            focusOnTime = DateTime.Now;
            TimeSpan timeSpan = focusOnTime - focusOffTime;
            _localTimer.m_duration -= timeSpan.Seconds;
        }
    }
}

public enum SpecialOfferType
{
    Special,
    Casual,
    Exclusive,
}
