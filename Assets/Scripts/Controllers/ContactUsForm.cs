using System.Net;
using System.Net.Mail;
using System.Security;
using Matchmania;
using UnityEngine;

public class ContactUsForm : MonoBehaviour
{
    [SerializeField] private ContactUsView contactUsView;
    [SerializeField] private string fromMailAddress = "matchmaniacontact@gmail.com";
    [SerializeField] private string toMailAddress = "matchmaniacontact@gmail.com";

    public void SendMessage()
    {
        var userMessage = contactUsView.GetInputText();
        if (userMessage.Length < 5)
        {
            Debug.LogWarning("The message doesn't have enough characters");
            contactUsView.ShowTextWarning();
            return;
        }
        MailMessage message = new MailMessage();  
        SmtpClient smtp = new SmtpClient();  
        message.From = new MailAddress(fromMailAddress);  
        message.To.Add(new MailAddress(toMailAddress));  
        message.Subject =
            $"Contact us form number {(char) Random.Range('a', 'z')}{(char) Random.Range('a', 'z')}{Random.Range(10, 100)}{Random.Range(10, 100)}{Random.Range(10, 100)}{(char) Random.Range('a', 'z')}{(char) Random.Range('a', 'z')}";  
        message.IsBodyHtml = true; //to make message body as html  
        message.Body = GenerateMessage(userMessage);  
        smtp.Port = 465;  
        smtp.Host = "smtp.gmail.com"; //for gmail host  
        smtp.EnableSsl = true;  
        smtp.UseDefaultCredentials = false;  
        smtp.Credentials = new NetworkCredential(fromMailAddress, GetPassword());  
        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;  
        smtp.Send(message);  
        contactUsView.ShowTextSuccess();
    }

    private SecureString GetPassword()
    {
        SecureString decryptedPass = new SecureString();
        var localDecryptedPass = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String("c2JsQlJeNzAjUW5jcG5WNw=="));
        foreach (var character in localDecryptedPass)
        {
            decryptedPass.AppendChar(character);
        }

        return decryptedPass;
    }

    private string GenerateMessage(string messageContents)
    {
        var generatedMessage = "";
        generatedMessage += "<h3><b>Message from contact form</b></h3>\n";
        generatedMessage += $"<p><b>Game name:</b> {Application.productName}\n</p>";
        generatedMessage += $"<p><b>Platform on which the game launched:</b> {Application.platform}\n</p>";
        generatedMessage += $"<p><b>The build version:</b> {Application.version}\n</p>";
        generatedMessage += $"<p><b>Is the game build intact (genuine non modified version):</b> {Application.genuine}\n</p>";
      
        generatedMessage += $"<p>User message:</b> {messageContents}\n</p>";
        generatedMessage += $"<h3>User in-game resources: {GenerateResourceTable()}\n</h3>";
        
        return generatedMessage;
    }

    private string GenerateResourceTable()
    {
        var generatedTableString = "";
        generatedTableString += "<table style='border-collapse: collapse; width: 100%; height: 108px;' border='1'>" +
                                "<tbody>" +
                                "<tr style='height: 18px;'>" +
                                "<td>Coins</td>" +
                                $"<td>{CurrencyController.Coins}</td>" +
                                "</tr>" +
                                "<tr style='height: 18px;'>" +
                                "<td>Fans</td>" +
                                $"<td>{CurrencyController.Fans}</td>" +
                                "</tr>" +
                                "<tr style='height: 18px;'>" +
                                "<td>Hearts</td>" +
                                $"<td>{CurrencyController.Hearts}</td>" +
                                "</tr>" +
                                "<tr style='height: 18px;'>" +
                                "<td>Hints</td>" +
                                $"<td>{CurrencyController.Hints}</td>" +
                                "</tr>" +
                                "<tr style='height: 18px;'>" +
                                "<td>Unlim hearts activated</td>" +
                                $"<td>{HeartsController.isUnlimited}</td>" +
                                "</tr>" +
                                "<tr style='height: 18px;'>" +
                                "<td>Stars</td>" +
                                $"<td>{CurrencyController.Stars}</td>" +
                                "</tr>" +
                                "</tbody>" +
                                "</table>" +
                                "";
        return generatedTableString;
    }
}
