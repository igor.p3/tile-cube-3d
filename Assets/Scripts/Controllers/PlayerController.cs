﻿using Matchmania;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Matchmania
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float speed = 15f;
        [SerializeField] private LayerMask layerMask;
        [SerializeField] private SFXConfig SFXConfig;

        public static List<List<ItemController>> selectedItems = new List<List<ItemController>>();

        private void Start()
        {
        
        }

        public static void ClearSelectedItems()
        {
            selectedItems.Clear();
            for (int i = 0; i < 20; i++)
            {
                List<ItemController> items = new List<ItemController>();
                selectedItems.Add(items);
            }
        }
        void Update()
        {
            PlayerTurn();
        }
        private void PlayerTurn()
        {
            RaycastHit hit;

            var tapCount = Input.touchCount;
            for (var k = 0; k < tapCount; k++)
            {
                var touch = Input.GetTouch(k);
                ItemController item = null;
                Transform itemTramsform = null;

                Ray ray = Camera.main.ScreenPointToRay(touch.position);

                if (Physics.Raycast(ray, out hit, 100f, layerMask))
                {
                    if (hit.collider.tag == "Item" && LevelController.Instance.IsCanMakeTurn && !hit.collider.GetComponent<ItemController>().isRotate)
                    {
                        if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Moved)
                        {
                            itemTramsform = hit.collider.transform;
                            item = hit.collider.GetComponent<ItemController>();
                            itemTramsform = hit.collider.transform;
                        
                            if (itemTramsform != null && itemTramsform == hit.collider.transform && LevelController.Instance.IsCanMakeTurn)
                            {
                                itemTramsform.GetComponent<ItemController>().SetSelectedItem(k);
                            }

                            if (touch.phase == TouchPhase.Ended)
                            {
                                if (item != null)
                                {
                                    ItemSlotController[] slots = LevelController.Instance.slots;

                                    item.DeselectItem();

                                    SFXConfig.PlaySoundEffect(SFXConfig.pickUpItem);

                                    for (int i = 0; i < slots.Length; i++)
                                    {
                                        if (slots[i].ItemController == null)
                                        {
                                            StartCoroutine(item.MoveToTarget(slots[i].transform.position, () =>
                                            {
                                                LevelController.Instance.CheckTurnResult();
                                            }));
                                            item.transform.DOScale(Vector3.one, 0.25f);
                                            slots[i].ItemController = item;
                                            item.transform.SetParent(slots[i].transform);
                                            break;
                                        }
                                        else if (slots[i].ItemController != null && slots[i].ItemController.name == item.name)
                                        {
                                            for (int j = slots.Length - 1; j > i; j--)
                                            {
                                                if (slots[j].ItemController != null)
                                                {
                                                    if (j + 1 < slots.Length)
                                                    {
                                                        StartCoroutine(slots[j].ItemController.MoveToTarget(slots[j + 1].transform.position));
                                                        item.transform.DOScale(Vector3.one, 0.25f);
                                                        slots[j + 1].ItemController = slots[j].ItemController;
                                                        item.transform.SetParent(slots[j + 1].transform);
                                                    }
                                                    else return;
                                                }
                                            }
                                            if (i + 1 <= slots.Length - 1)
                                            {
                                                StartCoroutine(item.MoveToTarget(slots[i + 1].transform.position, () =>
                                                {
                                                    LevelController.Instance.CheckTurnResult();
                                                }));
                                                item.transform.DOScale(Vector3.one, 0.25f);
                                                slots[i + 1].ItemController = item;
                                                item.transform.SetParent(slots[i + 1].transform);

                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            GameController.SaveGameData();
                        }
                    }
                    else if (hit.collider.tag != "Item")
                    {
                        if (selectedItems != null && selectedItems.Count != 0)
                        {
                            for (int i = 0; i < selectedItems[k].Count; i++)
                            {
                                if (selectedItems[k] != null)
                                {
                                    if (selectedItems[k][i] != null)
                                    {
                                        selectedItems[k][i].DeselectItem();
                                    }
                                }
                            }
                            selectedItems[k].Clear();
                        }
                    }
                }
            }
        }
    }
}