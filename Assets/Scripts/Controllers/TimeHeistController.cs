using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Matchmania
{
    public class TimeHeistController : MonoBehaviour
    {
        public static Action UpdateTimeHeistView;

        public static string stringTimeHeistDateTime;
        public static DateTime startTimeHeistDateTime;
        public static DateTime finishTimeHeistDateTime;
        public static DateTime nextTimeHeistStart;

        public static bool isTimeHeistEventEnabled;

        public static TimeHeistModel previousTimeHeistModel;
        public static TimeHeistModel currentTimeHeistModel;

        public static TimeHeistModel tempTimeHeistModel;

        void Start()
        {
            InitializedEvent();
        }
        public static void InitializedEvent()
        {
            if (string.IsNullOrEmpty(stringTimeHeistDateTime) == true || stringTimeHeistDateTime == "")
            {
                isTimeHeistEventEnabled = true;
                startTimeHeistDateTime = DateTime.Now;

                finishTimeHeistDateTime = startTimeHeistDateTime;
                finishTimeHeistDateTime = finishTimeHeistDateTime.AddHours(48);

                nextTimeHeistStart = startTimeHeistDateTime;
                nextTimeHeistStart = nextTimeHeistStart.AddHours(48);

                GameController.SaveGameData();
            }
            else
            {
                startTimeHeistDateTime = DateTime.Parse(stringTimeHeistDateTime);
                finishTimeHeistDateTime = startTimeHeistDateTime;
                finishTimeHeistDateTime = finishTimeHeistDateTime.AddHours(48);

                nextTimeHeistStart = startTimeHeistDateTime;
                nextTimeHeistStart = nextTimeHeistStart.AddHours(48);

                if (finishTimeHeistDateTime < DateTime.Now && DateTime.Now < nextTimeHeistStart)
                {
                    isTimeHeistEventEnabled = false;
                    return;
                }
                else if (DateTime.Now > nextTimeHeistStart)
                {
                    startTimeHeistDateTime = DateTime.Now;
                    finishTimeHeistDateTime = startTimeHeistDateTime;
                    finishTimeHeistDateTime = finishTimeHeistDateTime.AddHours(48);

                    nextTimeHeistStart = startTimeHeistDateTime;
                    nextTimeHeistStart = nextTimeHeistStart.AddHours(48);

                    CurrencyController.TimeHeistDiamonds = 0;

                    isTimeHeistEventEnabled = true;
                    previousTimeHeistModel = null;
                    GameController.SaveGameData();
                    UpdateTimeHeistView?.Invoke();
                    return;
                }
                else if (startTimeHeistDateTime > DateTime.Now)
                {
                    isTimeHeistEventEnabled = false;
                    return;
                }
                else
                {
                    isTimeHeistEventEnabled = true;
                    UpdateTimeHeistView?.Invoke();
                }
            }

        }

        public static void UpdateTimeHeistModel()
        {
            currentTimeHeistModel = GameModel.TimeHeistProgressions.TimeHeistProgressions[0];

            var models = GameModel.TimeHeistProgressions.TimeHeistProgressions;

            for (int i = 0; i < models.Length; i++)
            {
                if (CurrencyController.TimeHeistDiamonds >= models[i].diamondsCount)
                {
                    if (i < models.Length - 1)
                    {
                        previousTimeHeistModel = models[i];
                        currentTimeHeistModel = models[i + 1];
                    }
                    else
                    {
                        previousTimeHeistModel = models[i-1];
                        currentTimeHeistModel = models[i];
                    }
                }
            }

            if(tempTimeHeistModel == null)
            {
                tempTimeHeistModel = currentTimeHeistModel;
            }
            if(tempTimeHeistModel != currentTimeHeistModel)
            {
                tempTimeHeistModel = currentTimeHeistModel;
                TimeHeistView.GiveRewardEventHandler?.Invoke();
            }
        }
    } 
}
