using HyperCasualTemplate;
using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IPHONE
using Unity.Notifications.iOS;
#endif



namespace Matchmania
{
    public class PushNotificationController : MonoBehaviour
    {
        #region Singleton
        private static PushNotificationController _instance;
        public static PushNotificationController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<PushNotificationController>();
                }
                return _instance;
            }
        }
        #endregion
        private void Start()
        {
#if UNITY_ANDROID
            if (AndroidNotificationCenter.GetLastNotificationIntent() != null)
            {
                AnalyticsEvents.ExecuteEvent("StartGameFormNotification");
            }
#endif

#if UNITY_IOS
            if (iOSNotificationCenter.GetLastRespondedNotification() != null)
            {
                AnalyticsEvents.ExecuteEvent("StartGameFormNotification");
            }
#endif
            if (LevelController.CurrentLevelIndex >= 2)
            {
                PushNotificationController.Instance.RequestAuthorization();
            }
        }

        public void RequestAuthorization()
        {
#if UNITY_IOS
            StartCoroutine(iOS_RequestAuthorization());
#endif

#if UNITY_ANDROID
            Android_Authorization();
#endif


#if UNITY_ANDROID
            void Android_Authorization()
            {
                var channel = new AndroidNotificationChannel()
                {
                    Id = "channel_id",
                    Name = "Default Channel",
                    Importance = Importance.Default,
                    Description = "Generic notifications",
                };
                AndroidNotificationCenter.RegisterNotificationChannel(channel);

                var notification_01 = new AndroidNotification();
                notification_01.Title = "MatchMania 3D";
                notification_01.Text = "Are you smarter than your friends? Prove it now";
                notification_01.FireTime = System.DateTime.Now.AddHours(24);


                var notification_02 = new AndroidNotification();
                notification_02.Title = "MatchMania 3D";
                notification_02.Text = "Are you smarter than your friends? Prove it now";
                notification_02.FireTime = System.DateTime.Now.AddMinutes(30);


                var notification_03 = new AndroidNotification();
                notification_03.Title = "MatchMania 3D";
                notification_03.Text = "Are you smarter than your friends? Prove it now";
                notification_03.FireTime = System.DateTime.Now.AddHours(48);
 

                var notification_04 = new AndroidNotification();
                notification_04.Title = "MatchMania 3D";
                notification_04.Text = "Are you smarter than your friends? Prove it now";
                notification_04.FireTime = System.DateTime.Now.AddHours(96);


                var notification_05 = new AndroidNotification();
                notification_05.Title = "MatchMania 3D";
                notification_05.Text = "Are you smarter than your friends? Prove it now";
                notification_05.FireTime = System.DateTime.Now.AddHours(72);


                AndroidNotificationCenter.SendNotification(notification_01, "channel_id");
                AndroidNotificationCenter.SendNotification(notification_02, "channel_id");
                AndroidNotificationCenter.SendNotification(notification_03, "channel_id");
                AndroidNotificationCenter.SendNotification(notification_04, "channel_id");
                AndroidNotificationCenter.SendNotification(notification_05, "channel_id");
            }
#endif

        }

#if UNITY_IOS
        IEnumerator iOS_RequestAuthorization()
        {
            var authorizationOption = AuthorizationOption.Alert | AuthorizationOption.Badge;
            using (var req = new AuthorizationRequest(authorizationOption, true))
            {
                while (!req.IsFinished)
                {
                    yield return null;
                };

                string res = "\n RequestAuthorization:";
                res += "\n finished: " + req.IsFinished;
                res += "\n granted :  " + req.Granted;
                res += "\n error:  " + req.Error;
                res += "\n deviceToken:  " + req.DeviceToken;
                Debug.Log(res);

                if (req.Granted)
                {
                    SendNotification();
                }
            }
            AdvertisementSystem.isCanShowInactiveAdd = false;
        }
        static iOSNotificationTimeIntervalTrigger timeTrigger_01 = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = new TimeSpan(0, 30, 0),
            Repeats = false
        };
        static iOSNotificationTimeIntervalTrigger timeTrigger_02 = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = new TimeSpan(23, 0, 0),
            Repeats = true
        };

        iOSNotification notification_01 = new iOSNotification()
        {
            // You can specify a custom identifier which can be used to manage the notification later.
            // If you don't provide one, a unique string will be generated automatically.
            Identifier = "_notification_01",
            Title = "MatchMania 3D",
            Body = "Are you smarter than your friends? Prove it now",
            //Subtitle = "Want a smarter brain? Play now and be the best among your neighbors",
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = timeTrigger_01,
        };
        iOSNotification notification_02 = new iOSNotification()
        {
            // You can specify a custom identifier which can be used to manage the notification later.
            // If you don't provide one, a unique string will be generated automatically.
            Identifier = "_notification_02",
            Title = "MatchMania 3D",
            Body = "Are you smarter than your friends? Prove it now",
            //Subtitle = "Are you smarter than your friends? Prove it now",
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = timeTrigger_02,
        };

        public void SendNotification()
        {
            iOSNotificationCenter.ScheduleNotification(notification_01);
            iOSNotificationCenter.ScheduleNotification(notification_02);
        }
#endif
    }

}
