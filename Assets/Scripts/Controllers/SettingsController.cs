using UnityEngine;

namespace Matchmania
{
    public class SettingsController : MonoBehaviour
    {
        #region Singleton
        private static SettingsController _instance;
        public static SettingsController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<SettingsController>();
                }
                return _instance;
            }
        }
        #endregion

        public static bool isSoundOn = true;
        public static bool isMusicOn = true;
        public static bool isVibrationOn = true;

        private void Start()
        {
            if (isSoundOn == true)
            {
                AudioController.Instance.PlaySound(true);
            }
            else AudioController.Instance.PlaySound(false);

            if (isMusicOn == true)
            {
                AudioController.Instance.PlayMusic(true);
            }
            else AudioController.Instance.PlayMusic(false);
        }

        public void TurnOnSound()
        {
            if (isSoundOn)
            {
                isSoundOn = false;
                AudioController.Instance.PlaySound(false);
            }
            else
            {
                isSoundOn = true;
                AudioController.Instance.PlaySound(true);
            }

            GameController.SaveGameData();
        }
        public void TurnOnMusic()
        {
            if (isMusicOn)
            {
                isMusicOn = false;
                AudioController.Instance.PlayMusic(false);
            }
            else
            {
                isMusicOn = true;
                AudioController.Instance.PlayMusic(true);
            }

            GameController.SaveGameData();
        }
        public void TurnOnVibration()
        {
            isVibrationOn = !isVibrationOn;
            GameController.SaveGameData();
        }
    }
}