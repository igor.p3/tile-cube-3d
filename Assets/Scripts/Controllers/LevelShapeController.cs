using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Matchmania;

namespace TileCube3D
{
    public class LevelShapeController : MonoBehaviour
    {
        private float rotationSpeed = 15;

        public List<Transform> positions = new List<Transform>();
        public Transform[] layers;

        private float currentMouseX;
        private float currentMouseY;
        float startTime;

        private float dragTreshold = 0.15f;

        void Update()
        {
            RotateShape();
        }
        private void RotateShape()
        {
            if (Input.touchCount == 1 && LevelController.Instance.IsPlayingLevel)
            {
                var touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    startTime = Time.time;
                }
                if (touch.phase == TouchPhase.Moved)
                {
                    float timeDifference = Time.time - startTime;

                    if (timeDifference > dragTreshold)
                    {
                        currentMouseX = touch.deltaPosition.x;
                        currentMouseY = touch.deltaPosition.y;
                        transform.Rotate(currentMouseY * (rotationSpeed * Time.deltaTime), 0, -currentMouseX * (rotationSpeed * Time.deltaTime), Space.World);
                        LevelController.Instance.IsCanMakeTurn = false;
                    }
                }
                if (touch.phase == TouchPhase.Ended)
                {
                    LevelController.Instance.IsCanMakeTurn = true;
                }
            }
        }
    }
}