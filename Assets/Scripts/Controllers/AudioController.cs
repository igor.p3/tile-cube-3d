﻿using UnityEngine;

namespace Matchmania
{
    public class AudioController : MonoBehaviour
    {
        #region Singleton
        private static AudioController _instance;
        public static AudioController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<AudioController>();
                }

                return _instance;
            }
        }
        #endregion

        public AudioSource music;
        public AudioSource sfx;

        private float musicVolume;
        private float sfxVolume;

        private void Start()
        {
            musicVolume = music.volume;
            sfxVolume = sfx.volume;
        }
        public void PlaySound(bool isPlay)
        {
            if(isPlay)
            {
                sfx.volume = sfxVolume;
            }
            else
            {
                sfx.volume = 0;
            }
           
        }
        public void PlayMusic(bool isPlay)
        {
            if (isPlay)
            {
                music.volume = musicVolume;
            }
            else
            {
                music.volume = 0;
            }
        }
        public void TurnOnMusicAL(bool isTurnOn)
        {
            if (isTurnOn)
            {
                if(SettingsController.isMusicOn)
                {
                    PlayMusic(true);
                }
                if(SettingsController.isSoundOn)
                {
                    PlaySound(true);
                }
            }
            else
            {
                sfx.volume = 0;
                music.volume = 0;
            } 
        }
    }
}
