﻿using UnityEngine;

namespace HyperCasualTemplate
{
    public static class GameLevelsSystem
    {
        public static int CurrentLevelIndex { get; set; }
        private static GameObject currentGameLevel;

        public static GameObject LoadGameLevel(GameObject[] levelsPrefabs, int index)
        {
            UnloadLevel();
            GameObject gameLevel = GameObject.Instantiate(levelsPrefabs[index], Vector3.zero, Quaternion.identity);
            CurrentLevelIndex = index;
            return gameLevel;
        }
        public static void UnloadLevel()
        {
            if (currentGameLevel != null) GameObject.Destroy(currentGameLevel);
        }
    }
}