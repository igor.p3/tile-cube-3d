﻿using com.adjust.sdk;
using Firebase.Analytics;


namespace HyperCasualTemplate
{
    public static class AnalyticsEvents
    {
        public static void ExecuteEvent(string eventName)
        {
            FirebaseAnalytics.LogEvent(eventName);
        }
        public static void ExecuteEvent(string eventName, string parameterName, int value)
        {
            FirebaseAnalytics.LogEvent(eventName, new Parameter(parameterName, value));
        }
        public static void ExecuteAdjustEvent(string eventName)
        {
            AdjustEvent adjustEvent = new AdjustEvent(eventName);
            Adjust.trackEvent(adjustEvent);
        }
        public static void ExecuteRevenuetEvent(string eventName, double price)
        {
            AdjustEvent adjustEvent = new AdjustEvent(eventName);
            adjustEvent.setRevenue(price, "USD");
            Adjust.trackEvent(adjustEvent);
        }
    }
}