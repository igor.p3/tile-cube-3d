using UnityEngine;

namespace Matchmania
{
    public class PlayHoseSound : MonoBehaviour
    {
        public void PlayHoseSoundEffect()
        {
            ItemsSpawnController.Instance.PlayHoseSoundEffect();
        }
    }
}