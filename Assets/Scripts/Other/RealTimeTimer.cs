using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RealTimeTimer
{
    private static DateTime m_timeStarted;
    private DateTime m_timeFinished;
    private float m_duration;

    
    public bool IsDone => DateTime.Now > m_timeFinished;

    public float TimeLeft => m_duration - Elapsed;

    public float Elapsed => (float) (DateTime.Now - m_timeStarted).TotalSeconds;

    public RealTimeTimer( DateTime startMTime, float duration ) {
        m_timeStarted = startMTime;
        m_duration = duration;
        m_timeFinished = startMTime.AddSeconds(duration);
    }
}
