using GameAnalyticsSDK;
using UnityEngine;



public class GA_Manager : MonoBehaviour
{
    public static GA_Manager instance;
    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        GameAnalytics.Initialize();
    }
}
