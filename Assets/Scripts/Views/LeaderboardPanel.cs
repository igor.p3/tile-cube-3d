using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Matchmania
{
    public class LeaderboardPanel : MonoBehaviour
    {
     

        public Image cupImage;
        public Image flagImage;
        public Image background;

        public TextMeshProUGUI playerNameText;
        public TextMeshProUGUI playerLevelIndexText;
        public TextMeshProUGUI rankIndexText;
        public TextMeshProUGUI countryText;

        [Header("Sprites")] 
        [SerializeField] private Sprite basicHolderTexture;
        [SerializeField] private Sprite goldenHolderTexture;
        
        public void SetView(Sprite cup, Sprite flag, string playerName, int playerLevelIndex, int rankIndex, string playerCountry, bool isSelected)
        {
            cupImage.sprite = cup;
            flagImage.sprite = flag;
            playerNameText.text = playerName;
            playerLevelIndexText.text = playerLevelIndex.ToString();
            rankIndexText.text = rankIndex.ToString();
            countryText.text = playerCountry;

            if(cup == null)
            {
                cupImage.gameObject.SetActive(false);
            }
            else
            {
                cupImage.gameObject.SetActive(true);
            }
            if(flag == null)
            {
                flagImage.gameObject.SetActive(false);
                countryText.gameObject.SetActive(true);
            }
            else
            {
                flagImage.gameObject.SetActive(true);
                countryText.gameObject.SetActive(false);
            }

            background.sprite = isSelected ? goldenHolderTexture : basicHolderTexture;

        }
    }
}