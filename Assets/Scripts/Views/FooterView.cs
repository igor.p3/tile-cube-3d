using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using TMPro;

namespace Matchmania
{
    public class FooterView : MonoBehaviour
    {
        public enum CurrentScreenType
        {
            home,
            shop,
            leaderboard,
            level
        }
        private Vector2 fingerDownPos;
        private Vector2 fingerStartCurrentPos;
        private Vector2 fingerCurrentPos;
        private Vector2 fingerUpPos;
        private bool detectSwipeAfterRelease = true;
        private float SWIPE_THRESHOLD = 150f;
        public static bool isCanSwipe = true;

        public static CurrentScreenType currentScreenType;

        [SerializeField] private ScrollRect shopScroll;
        [SerializeField] private ScrollRect leaderboardScroll;

        [SerializeField] private Button shopButton;
        [SerializeField] private Button homeButton;
        [SerializeField] private Button leaderboardButton;

        [SerializeField] private GameObject leaderboardMainMenuButton;

        [SerializeField] private GameObject shopSelection;
        [SerializeField] private GameObject homeSelection;
        [SerializeField] private GameObject leaderboardSelection;

        public GameObject footerView;

        [SerializeField] RectTransform shopScreen;
        [SerializeField] RectTransform mainMenuScreen;
        [SerializeField] RectTransform leaderboardScreen;

        [SerializeField] RectTransform currentScreen;

        [SerializeField] RectTransform shopIcon;
        [SerializeField] RectTransform mainMenuIcon;
        [SerializeField] RectTransform leaderboardIcon;

        [SerializeField] RectTransform selectionTab;

        [SerializeField] RectTransform shopSelectionRectTransform;
        [SerializeField] RectTransform homeSelectionRectTransform;
        [SerializeField] RectTransform leaderboardSelectionRectTransform;

        [SerializeField] private LeaderBoardView LeaderBoardView;
        [SerializeField] private ChangeNicknameView ChangeNicknameView;

        private float screenMovementDuration = 0.35f;
        private float outScreenPos = 2500;
        private float swipteTrashhold = 150f;

        Vector3 position;
        Ease easeType = Ease.OutCirc;

        void Start()
        {
            leaderboardMainMenuButton.SetActive(false);

            shopButton.onClick.AddListener(ShopButtonPressed);
            homeButton.onClick.AddListener(HomeButtonPressed);
            leaderboardButton.onClick.AddListener(LeaderboardButtonPressed);

            shopSelection.SetActive(false);
            homeSelection.SetActive(true);
            leaderboardSelection.SetActive(false);

            currentScreen = mainMenuScreen;

        }
        // Update is called once per frame
        void Update()
        {
            if (isCanSwipe == true && TimeHeistView.isGivingReward == false)
            {
                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        fingerUpPos = touch.position;
                        fingerDownPos = touch.position;
                        fingerStartCurrentPos = touch.position;
                    }

                    //Detects Swipe while finger is still moving on screen
                    if (touch.phase == TouchPhase.Moved)
                    {
                        if (!detectSwipeAfterRelease)
                        {
                            fingerDownPos = touch.position;

                            DetectSwipe();
                        }

                        fingerCurrentPos = touch.position;
                        position = Vector3.zero;

                     

                        if (HorizontalCurrentMoveValue() < -swipteTrashhold)
                        {
                            leaderboardScroll.enabled = false;
                            shopScroll.enabled = false;
                            position.x = HorizontalCurrentMoveValue()  + swipteTrashhold;
                        }
                        else if(HorizontalCurrentMoveValue() > swipteTrashhold)
                        {
                            leaderboardScroll.enabled = false;
                            shopScroll.enabled = false;
                            position.x = HorizontalCurrentMoveValue() - swipteTrashhold;
                        }
                        else
                        {
                            leaderboardScroll.enabled = true;
                            shopScroll.enabled = true;
                        }
 

                        currentScreen.anchoredPosition = position;
                    }

                    //Detects swipe after finger is released from screen
                    if (touch.phase == TouchPhase.Ended)
                    {
                        fingerDownPos = touch.position;
                        DetectSwipe();
                    }
                }
            }
        }

        
        void DetectSwipe()
        {

            //if (VerticalMoveValue() > SWIPE_THRESHOLD && VerticalMoveValue() > HorizontalMoveValue())
            //{
            //    //Debug.Log("Vertical Swipe Detected!");
            //    if (fingerDownPos.y - fingerUpPos.y > 0)
            //    {
            //        OnSwipeUp();
            //    }
            //    else if (fingerDownPos.y - fingerUpPos.y < 0)
            //    {
            //        OnSwipeDown();
            //    }
            //    fingerUpPos = fingerDownPos;

            //}
             if (HorizontalMoveValue() > SWIPE_THRESHOLD && HorizontalMoveValue() > VerticalMoveValue())
            {
                //Debug.Log("Horizontal Swipe Detected!");
                if (fingerDownPos.x - fingerUpPos.x > 0)
                {

                    OnSwipeRight();
                }
                else if (fingerDownPos.x - fingerUpPos.x < 0)
                {
                    OnSwipeLeft();
                }
                fingerUpPos = fingerDownPos;

                if (currentScreenType == CurrentScreenType.shop && HorizontalCurrentMoveValue() > 0)
                {
                    currentScreen.DOAnchorPosX(0, 0.1f);
                }
                else if (currentScreenType == CurrentScreenType.leaderboard && HorizontalCurrentMoveValue() < 0)
                {
                    currentScreen.DOAnchorPosX(0, 0.1f);
                }

            }
            else
            {
                //Debug.Log("No Swipe Detected!");
                currentScreen.DOAnchorPosX(0, 0.1f);
            }

           
        }

        public void OpenShop()
        {
 
            isCanSwipe = false;
            Vector3 screenPos = shopScreen.localPosition;
            shopScreen.localScale = Vector3.one;
            screenPos.x = -outScreenPos;
            shopScreen.localPosition = screenPos;
            shopScreen.gameObject.SetActive(true);
            var mainMove = shopScreen.DOLocalMoveX(0, screenMovementDuration);
            mainMove.SetEase(easeType);

            if (shopScreen.GetComponent<CanvasGroup>())
                shopScreen.GetComponent<CanvasGroup>().alpha = 1;

            shopScreen.transform.GetChild(0).GetComponent<Image>().DOFade(0, 0);
            shopScreen.transform.GetChild(0).GetComponent<Image>().DOFade(1, 0.5f);
            //shopScreen.transform.GetChild(0).gameObject.SetActive(true);

            if (currentScreenType == CurrentScreenType.home)
            {
                var move = mainMenuScreen.DOLocalMoveX(outScreenPos, screenMovementDuration).OnComplete(() => mainMenuScreen.gameObject.SetActive(false));
                move.SetEase(easeType).OnComplete(() => {
                    isCanSwipe = true;
                    currentScreenType = CurrentScreenType.shop;
                    currentScreen = shopScreen;
                }
                );
            }
            else if (currentScreenType == CurrentScreenType.leaderboard)
            {
                var move = leaderboardScreen.DOLocalMoveX(outScreenPos, screenMovementDuration).OnComplete(() => mainMenuScreen.gameObject.SetActive(false));
                move.SetEase(easeType).OnComplete(() => {
                    isCanSwipe = true;
                    currentScreenType = CurrentScreenType.shop;
                    currentScreen = shopScreen;
                }
                );
            }



            shopSelection.SetActive(true);
            homeSelection.SetActive(false);
            leaderboardSelection.SetActive(false);

            var movement = selectionTab.DOMoveX(shopSelectionRectTransform.position.x, 0.35f);
            movement.SetEase(easeType);

            shopIcon.DOLocalMoveY(75, 0f);
            shopIcon.DOLocalMoveX(-75, 0f);
            shopIcon.DOLocalMoveX(0, 0.25f);
            mainMenuIcon.DOLocalMoveY(0, 0);
            leaderboardIcon.DOLocalMoveY(0, 0);

            ChangeNicknameView.CloseButtonPressed();
        }
        public void ShopButtonPressed()
        {
            if (currentScreenType == CurrentScreenType.shop || TimeHeistView.isGivingReward || !isCanSwipe) return;

            OpenShop();
        }
        public void HomeButtonPressed()
        {
            if (currentScreenType == CurrentScreenType.home || !isCanSwipe || TimeHeistView.isGivingReward) return;

            isCanSwipe = false;
            if (currentScreenType == CurrentScreenType.shop)
            {
                Vector3 screenPos = mainMenuScreen.localPosition;
                mainMenuScreen.localScale = Vector3.one;
                screenPos.x = outScreenPos;
                mainMenuScreen.localPosition = screenPos;
                mainMenuScreen.gameObject.SetActive(true);
                var move1 = mainMenuScreen.DOLocalMoveX(0, screenMovementDuration);
                var move2 = shopScreen.DOLocalMoveX(-outScreenPos, screenMovementDuration).OnComplete(() =>
                {
                    shopScreen.gameObject.SetActive(false);
                    isCanSwipe = true;
                    currentScreenType = CurrentScreenType.home;
                    currentScreen = mainMenuScreen;
                });
                shopScroll.enabled = true;
                move1.SetEase(easeType);
                move2.SetEase(easeType);
            }
            else if (currentScreenType == CurrentScreenType.leaderboard)
            {
                Vector3 screenPos = mainMenuScreen.localPosition;
                mainMenuScreen.localScale = Vector3.one;
                screenPos.x = -outScreenPos;
                mainMenuScreen.localPosition = screenPos;
                mainMenuScreen.gameObject.SetActive(true);
                var move1 = mainMenuScreen.DOLocalMoveX(0, screenMovementDuration);
                var move2 = leaderboardScreen.DOLocalMoveX(outScreenPos, screenMovementDuration).OnComplete(() =>
                {
                    leaderboardScreen.gameObject.SetActive(false);
                    isCanSwipe = true;
                    currentScreenType = CurrentScreenType.home;
                    currentScreen = mainMenuScreen;

                });

                move1.SetEase(easeType);
                move2.SetEase(easeType);
            }
            currentScreenType = CurrentScreenType.home;
            currentScreen = mainMenuScreen;
            var movement = selectionTab.DOMoveX(homeSelectionRectTransform.position.x, 0.35f);
            movement.SetEase(easeType);
            shopSelection.SetActive(false);
            homeSelection.SetActive(true);
            leaderboardSelection.SetActive(false);

            shopIcon.DOLocalMoveY(0, 0f);
            leaderboardIcon.DOLocalMoveY(0, 0f);

            mainMenuIcon.DOLocalMoveY(75, 0f);
            mainMenuIcon.DOLocalMoveX(-75, 0f);
            mainMenuIcon.DOLocalMoveX(0, 0.25f);

            ChangeNicknameView.CloseButtonPressed();

            shopScreen.transform.GetChild(0).transform.localScale = Vector3.one;
        }
        public void LeaderboardButtonPressed()
        {
            if (currentScreenType == CurrentScreenType.leaderboard || TimeHeistView.isGivingReward || !isCanSwipe) return;

            isCanSwipe = false;
            Vector3 screenPos = leaderboardScreen.localPosition;
            leaderboardScreen.localScale = Vector3.one;
            screenPos.x = outScreenPos;
            leaderboardScreen.localPosition = screenPos;
            leaderboardScreen.gameObject.SetActive(true);
            var mainMove = leaderboardScreen.DOLocalMoveX(0, screenMovementDuration);
            mainMove.SetEase(easeType);

            var move = mainMenuScreen.DOLocalMoveX(-outScreenPos, screenMovementDuration).OnComplete(() =>
            {
                mainMenuScreen.gameObject.SetActive(false);
                isCanSwipe = true;
                currentScreenType = CurrentScreenType.leaderboard;
                currentScreen = leaderboardScreen;
            });
            move.SetEase(easeType);


            var move_02 = shopScreen.DOLocalMoveX(-outScreenPos, screenMovementDuration).OnComplete(() =>
            {
                mainMenuScreen.gameObject.SetActive(false);
                isCanSwipe = true;
                currentScreenType = CurrentScreenType.leaderboard;
                currentScreen = leaderboardScreen;
            });
            move_02.SetEase(easeType);

            shopScreen.transform.GetChild(0).transform.localScale = Vector3.one;


            selectionTab.DOMoveX(leaderboardSelectionRectTransform.position.x, 0.35f);
            shopSelection.SetActive(false);
            homeSelection.SetActive(false);
            leaderboardSelection.SetActive(true);

            shopIcon.DOLocalMoveY(0, 0f);
            mainMenuIcon.DOLocalMoveY(0, 0);
            leaderboardIcon.DOLocalMoveY(75, 0f);
            leaderboardIcon.DOLocalMoveX(-75, 0f);
            leaderboardIcon.DOLocalMoveX(0, 0.25f);

            LeaderBoardView.WorldSortButtonPressed();
        }

        #region Swipe
        float VerticalMoveValue()
        {
            return Mathf.Abs(fingerDownPos.y - fingerUpPos.y);
        }

        float HorizontalMoveValue()
        {
            return Mathf.Abs(fingerDownPos.x - fingerUpPos.x);
        }
        float HorizontalCurrentMoveValue()
        {
            float value = Mathf.Abs(fingerCurrentPos.x - fingerStartCurrentPos.x);

            if (fingerCurrentPos.x > fingerStartCurrentPos.x )
            {
                value = value;
            }
            else if (fingerCurrentPos.x < fingerStartCurrentPos.x)
            {
                value = -value;
            }
            return value;

            //{
            //    if (touch.phase == TouchPhase.Moved)
            //    {
            //        if ()
            //    }

            //}
        }

        void OnSwipeUp()
        {
            //Do something when swiped up
        }

        void OnSwipeDown()
        {
            //Do something when swiped down
        }

        void OnSwipeLeft()
        {
            if (currentScreenType == CurrentScreenType.home)
            {
                LeaderboardButtonPressed();
            }
            else if (currentScreenType == CurrentScreenType.shop)
            {
                HomeButtonPressed();
            }

        }

        void OnSwipeRight()
        {
            if (currentScreenType == CurrentScreenType.home)
            {
                ShopButtonPressed();
            }
            else if (currentScreenType == CurrentScreenType.leaderboard)
            {
                HomeButtonPressed();
            }

        }
        #endregion

    }


}
