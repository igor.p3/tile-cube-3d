using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace Matchmania
{
    public class VictoryScreenView : MonoBehaviour
    {
        [SerializeField] private NewOpenedItemsView newOpenedItemsView;
        [SerializeField] private LevelView levelView;

        [SerializeField] private Slider itemsProgressionSlider;
        [SerializeField] private Slider fullScreenItemsProgressionSlider;
        [SerializeField] private TextMeshProUGUI itemsProgressionText;
        [SerializeField] private TextMeshProUGUI fullScreenItemsProgressionText;

        [SerializeField] private GameObject itemsProgressionTab;
        [SerializeField] private GameObject fullScreenProgressionTab;
        [SerializeField] private RectTransform texts;
        [SerializeField] private RectTransform starsHolder;

        [SerializeField] private GameObject oneButtonTab;
        [SerializeField] private GameObject twoButtonsTab;

        [SerializeField] private GameObject victorScreenTab;

        [SerializeField] private Button nextLevelButton;
        [SerializeField] private Button homeButton;

        public static bool isShownNewItemsView;

        void OnEnable()
        {
            if (!isShownNewItemsView)
            {
                CheckView();
                isShownNewItemsView = true;
            }
            nextLevelButton.onClick.AddListener(levelView.NextLevelButtonPressed);
            homeButton.onClick.AddListener(levelView.OnGetMainMenu);
        }
        void OnDisable()
        {
            nextLevelButton.onClick.RemoveAllListeners();
            homeButton.onClick.RemoveAllListeners();
        }
        private void CheckView()
        {
            PacksProgressionModel nextProgression = null;
            PacksProgressionModel currentProgression = null;

            CheckButtons();

            foreach (var pack in GameModel.Packs.Packs)
            {
                if (pack.levelComplete >= LevelController.CurrentLevelIndex)
                {
                    nextProgression = pack;
                    break;
                }
                else
                {
                    currentProgression = pack;
                }
            }
            if (nextProgression != null)
            {
                texts.localPosition = new Vector3(0, 276, 0);
                starsHolder.localPosition = new Vector3(0, -156, 0);
                if (LevelController.CurrentLevelIndex == 1)
                {
                    itemsProgressionSlider.DOValue(20, 1f).OnUpdate(UpdateText);

                    return;
                }

                float levelDifference = nextProgression.levelComplete - currentProgression.levelComplete;

                float currentLevelDifference = nextProgression.levelComplete - LevelController.CurrentLevelIndex;

                float percents = ((currentLevelDifference) / levelDifference) * 100;

                percents = (-100 + percents) * -1;

                if (currentLevelDifference == 0)
                {

                    LevelView.OpenScreen(fullScreenProgressionTab, 1f);
                    fullScreenItemsProgressionSlider.value = 90;
                    UpdateText();
                    fullScreenItemsProgressionSlider.DOValue(100, 1.5f).OnComplete(OpenProgressionItemsScreen).OnUpdate(UpdateText);
                    itemsProgressionTab.SetActive(false);
                    texts.localPosition = new Vector3(0, 500, 0);
                    starsHolder.localPosition = new Vector3(0, -46, 0);
                    return;
                }
                itemsProgressionTab.SetActive(true);
                UpdateText();
                itemsProgressionSlider.DOValue(percents, 1f).OnComplete(OpenProgressionItemsScreen).OnUpdate(UpdateText);
            }
            else
            {
                itemsProgressionTab.SetActive(false);
            }
        }

        private void UpdateText()
        {
            itemsProgressionText.text = Mathf.RoundToInt(itemsProgressionSlider.value).ToString() + "%";
            fullScreenItemsProgressionText.text = Mathf.RoundToInt(fullScreenItemsProgressionSlider.value).ToString() + "%";
        }
        private void OpenProgressionItemsScreen()
        {
            if (itemsProgressionSlider.value == 100 || fullScreenItemsProgressionSlider.value == 100)
            {
                victorScreenTab.SetActive(false);
                itemsProgressionSlider.value = 0;
                fullScreenItemsProgressionSlider.value = 0;
                UpdateText();
                LevelView.OpenScreen(newOpenedItemsView.openedItemsScreen, 1f);


                newOpenedItemsView.SetItemsView(GetIconsPack());

                LevelView.CloseScreen(fullScreenProgressionTab);
            }


        }
        private void CheckButtons()
        {
            //if (CurrencyController.LevelsChestCount >= 10 || CurrencyController.StarsChestCount >= 1000 || TimeHeistController.isTimeHeistEventEnabled)
            //{
            //    oneButtonTab.SetActive(true);
            //    twoButtonsTab.SetActive(false);
            //}
            //else
            //{
            //    oneButtonTab.SetActive(false);
            //    twoButtonsTab.SetActive(true);
            //}
            twoButtonsTab.SetActive(true);
        }
        public static Sprite[] GetIconsPack()
        {
            Sprite[] sprites = null;

            foreach (var item in GameModel.Packs.Packs)
            {
                if (item.levelComplete == LevelController.CurrentLevelIndex)
                {
                    if (item.packName == "animals")
                    {
                        sprites = GameController.animalsIcons;
                    }
                    else if (item.packName == "sweets")
                    {
                        sprites = GameController.sweetsIcons;
                    }
                    else if (item.packName == "fruits")
                    {
                        sprites = GameController.fruitsIcons;
                    }
                    else if (item.packName == "food")
                    {
                        sprites = GameController.foodIcons;
                    }
                    else if (item.packName == "toys")
                    {
                        sprites = GameController.toysIcons;
                    }
                    else if (item.packName == "animals_toys_sweets_food_drinks_plants")
                    {
                        sprites = GameController.animals_toys_sweets_food_drinks_plantsIcons;
                    }
                }
            }
            return sprites;
        }
    }
}