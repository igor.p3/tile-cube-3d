using UnityEngine.UI;
using UnityEngine;
using TMPro;

namespace Matchmania
{
    public class FakeLeaderboardTabView : MonoBehaviour
    {
        public int score;
        public int place;

        [Header("Images")]

        public Image flagImage;

        [Header("Texts")]

        public TextMeshProUGUI playerScoreText;
        public TextMeshProUGUI playerPlace;
        public TextMeshProUGUI playerNameText;

        public void SetView()
        {
            playerScoreText.text = score.ToString();
            playerPlace.text = place.ToString();

            flagImage.sprite = FakeLeaderboardController.GenerateCountry();
            playerNameText.text = FakeLeaderboardController.NameGenerator();
        }
    }
}