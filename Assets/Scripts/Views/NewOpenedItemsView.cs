using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Matchmania
{
    public class NewOpenedItemsView : MonoBehaviour
    {
        public GameObject openedItemsScreen;
        [SerializeField] private Image[] itemsPositions;
        [SerializeField] private Button getItemsButton;
        [SerializeField] private GameObject victorScreenTab;

        private bool isShowingItems;

        private void Start()
        {
            getItemsButton.onClick.AddListener(GetItemsButtonPressed);
        }
        private void GetItemsButtonPressed()
        {
            if (isShowingItems == false)
            {
                LevelView.CloseScreen(openedItemsScreen);
                victorScreenTab.SetActive(true);


                if (LevelController.CurrentLevelIndex == 10)
                {
                    RateUsView.Instance.ShowRateUsScreen();
                }
                ClearItems();
            }
        }
        public void SetItemsView(Sprite[] spritesToShow)
        {
            ClearItems();
            StartCoroutine(ShowItems(spritesToShow));

        }
        IEnumerator ShowItems(Sprite[] spritesToShow)
        {
            isShowingItems = true;

            for (int i = 0; i < itemsPositions.Length; i++)
            {
                itemsPositions[i].sprite = spritesToShow[i];
                itemsPositions[i].transform.DOScale(1, 0.25f);
                yield return new WaitForSeconds(0.1f);
            }
            isShowingItems = false;
        }
        private void ClearItems()
        {
            foreach (var item in itemsPositions)
            {
                item.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            }
        }
    }
}