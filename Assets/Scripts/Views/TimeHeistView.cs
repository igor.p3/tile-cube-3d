using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
using System.Collections;

namespace Matchmania
{
    public class TimeHeistView : MonoBehaviour
    {
        public static Action UpdateTimeHeistViewEventHandler;
        public static Action GiveRewardEventHandler;

        [SerializeField] private RectTransform playButtonRect;
        [SerializeField] private RectTransform walletRect;
        [SerializeField] private RectTransform sliderRewardRect;

        [SerializeField] private RectTransform hintRewardRect;
        [SerializeField] private RectTransform fanRewardRect;
        [SerializeField] private RectTransform hintRewardTargetRect;
        [SerializeField] private RectTransform fanRewardTargetRect;

        [Header("References")]

        [SerializeField] private MainMenuView MainMenuView;

        [Header("Screens")]

        [SerializeField] private GameObject timeHeistDialog;
        [SerializeField] private GameObject rewardScreen;
        [SerializeField] private GameObject timeHeistProgressBar;

        [SerializeField] private GameObject boostersRewardView;
        [SerializeField] private GameObject coinsRewardView;
        [SerializeField] private GameObject heartsRewardView;

        [SerializeField] private GameObject dialogBoostersRewardView;
        [SerializeField] private GameObject dialogCoinsRewardView;
        [SerializeField] private GameObject dialogHeartsRewardView;

        [SerializeField] private Canvas footerCanvas;
        [SerializeField] private Canvas shopCanvas;

        [Header("Texts")]

        [SerializeField] private TextMeshProUGUI dialogTimer;
        [SerializeField] private TextMeshProUGUI progressBarTimer;
        [SerializeField] private TextMeshProUGUI dialogNextRewardAmount;
        [SerializeField] private TextMeshProUGUI progressBarNextRewardAmount;
        [SerializeField] private TextMeshProUGUI dialogCurrentProgress;
        [SerializeField] private TextMeshProUGUI progressBarCurrentProgress;
        [SerializeField] private TextMeshProUGUI hintRewardTextCount;
        [SerializeField] private TextMeshProUGUI fanRewardTextCount;
        [SerializeField] private TextMeshProUGUI dialogHeartsRewardTextCount;
        [SerializeField] private TextMeshProUGUI progressBarHeartsRewardTextCount;
        [SerializeField] private TextMeshProUGUI descriptionText;

        [Header("Buttons")]

        [SerializeField] private Button openTimeHeistDialog;
        [SerializeField] private Button closeTimeHeistDialog;
        [SerializeField] private Button claimReward;

        [Header("Images")]

        [SerializeField] private RectTransform rewardImage;
        [SerializeField] private Image giftImage;

        [Header("Sprites")]

        [SerializeField] private Sprite giftClosedSprite;
        [SerializeField] private Sprite giftOpenedSprite;

        [Header("Sliders")]

        [SerializeField] private Slider dialogProgressionSlider;
        [SerializeField] private Slider progressBarProgressionSlider;

        private Vector3 rewardIconStartPos;
        public static bool isGivingReward;

        private Tweener sliderTweener;

        void Start()
        {
            LevelController.StartLevelEventHandler += (() => timeHeistProgressBar.SetActive(false));
            LevelView.GoHomeEventHandler += CheckTimeHeistEvent;
            UpdateTimeHeistViewEventHandler += UpdateTimeHeistView;
            GiveRewardEventHandler += OpenRewardScreen;

            openTimeHeistDialog.onClick.AddListener(OpenTimeHeistDialogButtonPressed);
            closeTimeHeistDialog.onClick.AddListener(CloseTimeHeistDialogButtonPRessed);
            claimReward.onClick.AddListener(GiveReward);

            UpdateTimeHeistView();
            UpdateRewardIcon();
            CheckTimeHeistEvent();

            TimeHeistController.UpdateTimeHeistView += UpdateTimeHeistView;
            //TimeHeistController.UpdateTimeHeistView += UpdateRewardIcon;

            rewardIconStartPos = rewardImage.position;

        }
        private void OpenTimeHeistDialogButtonPressed()
        {
            LevelView.OpenScreen(timeHeistDialog);
            descriptionText.text = $"Collect {progressBarProgressionSlider.maxValue - progressBarProgressionSlider.value} Diamonds to Win The reward!";
        }
        private void CloseTimeHeistDialogButtonPRessed()
        {
            LevelView.CloseScreen(timeHeistDialog);
           
        }
        private void CheckTimeHeistEvent()
        {
            if (TimeHeistController.isTimeHeistEventEnabled)
            {
                timeHeistProgressBar.SetActive(true);
            }
            else
            {
                timeHeistProgressBar.SetActive(false);
            }
        }
        private void Update()
        {
            if (TimeHeistController.isTimeHeistEventEnabled)
            {
                UpdateTimer();
            }

            //if (Input.GetMouseButtonDown(0))
            //{
            //    OpenRewardScreen();
            //}
        }
        private void UpdateTimer()
        {
            TimeSpan timeSpan = TimeHeistController.finishTimeHeistDateTime - DateTime.Now;

            if (DateTime.Now > TimeHeistController.finishTimeHeistDateTime)
            {
                TimeHeistController.InitializedEvent();
            }
            else
            {
                dialogTimer.text = $"{(int)timeSpan.TotalHours}{timeSpan.ToString(@"\:mm\:ss")}";
                progressBarTimer.text = $"{(int)timeSpan.TotalHours}{timeSpan.ToString(@"\:mm\:ss")}";
            }
        }
        private void UpdateTimeHeistView()
        {
            TimeHeistController.UpdateTimeHeistModel();

            int sliderValue = CurrencyController.TimeHeistDiamonds;

            int sliderMaxValue = TimeHeistController.currentTimeHeistModel.diamondsCount;


            if (TimeHeistController.previousTimeHeistModel != null)
            {
                sliderValue = CurrencyController.TimeHeistDiamonds - TimeHeistController.previousTimeHeistModel.diamondsCount;
                sliderMaxValue = TimeHeistController.currentTimeHeistModel.diamondsCount - TimeHeistController.previousTimeHeistModel.diamondsCount;
            }

            dialogProgressionSlider.maxValue = sliderMaxValue;
            dialogProgressionSlider.value = sliderValue;

            progressBarProgressionSlider.maxValue = sliderMaxValue;

            sliderTweener = progressBarProgressionSlider.DOValue(sliderValue, 0.3f).OnComplete(() =>
            {
                dialogCurrentProgress.text = ((int)dialogProgressionSlider.value).ToString() + "/" + dialogProgressionSlider.maxValue;
                progressBarCurrentProgress.text = ((int)progressBarProgressionSlider.value).ToString() + "/" + progressBarProgressionSlider.maxValue;

            });
            GameController.SaveGameData();
        }
        private void UpdateRewardIcon()
        {
            if (TimeHeistController.currentTimeHeistModel.rewardType == "Coins")
            {
                coinsRewardView.SetActive(true);
                dialogCoinsRewardView.SetActive(true);
                boostersRewardView.SetActive(false);
                dialogBoostersRewardView.SetActive(false);
                heartsRewardView.SetActive(false);
                dialogHeartsRewardView.SetActive(false);

                dialogNextRewardAmount.text = TimeHeistController.currentTimeHeistModel.rewardCount.ToString();
                progressBarNextRewardAmount.text = TimeHeistController.currentTimeHeistModel.rewardCount.ToString();
            }
            else if (TimeHeistController.currentTimeHeistModel.rewardType == "Boosters")
            {
                coinsRewardView.SetActive(false);
                dialogCoinsRewardView.SetActive(false);
                boostersRewardView.SetActive(true);
                dialogBoostersRewardView.SetActive(true);
                heartsRewardView.SetActive(false);
                dialogHeartsRewardView.SetActive(false);
            }
            else if (TimeHeistController.currentTimeHeistModel.rewardType == "Hearts")
            {
                coinsRewardView.SetActive(false);
                dialogCoinsRewardView.SetActive(false);
                boostersRewardView.SetActive(false);
                dialogBoostersRewardView.SetActive(false);
                heartsRewardView.SetActive(true);
                dialogHeartsRewardView.SetActive(true);

                dialogHeartsRewardTextCount.text = $"{TimeHeistController.currentTimeHeistModel.rewardCount} min";
                progressBarHeartsRewardTextCount.text = $"{TimeHeistController.currentTimeHeistModel.rewardCount} min"; 
            }
        }
        private void OpenRewardScreen()
        {
            MainMenuView.diomandsFly.StopAllCoroutines();
            if(MainMenuView.diamondsTweener != null) MainMenuView.diamondsTweener.Kill();
            isGivingReward = true;
            LevelView.OpenScreen(rewardScreen);
            footerCanvas.sortingOrder = 6;
             shopCanvas.sortingOrder = 6;

            claimReward.gameObject.SetActive(true);

            var sequence = rewardImage.DOJumpAnchorPos(new Vector2(0, -500), 0.25f, 1, 0.75f, false)
                   .Join(rewardImage.transform.DOScale(4f, 0.75f)
                   .SetEase(Ease.InCubic)
                   .Play());

        }
        private void GiveReward()
        {
            isGivingReward = true;

             claimReward.gameObject.SetActive(false);

            Vector3 targetPos = sliderRewardRect.position;

            if (TimeHeistController.previousTimeHeistModel == null)
            {
                TimeHeistController.previousTimeHeistModel = TimeHeistController.currentTimeHeistModel;
            }

            if (TimeHeistController.previousTimeHeistModel.rewardType == "Coins")
            {
                MakeImpuleAnimation(rewardImage, () =>
                {
                    LevelView.CloseScreen(rewardScreen);
                    var sequence = rewardImage.DOJump(targetPos, 0.25f, 1, 0.75f, false)
                       .Join(rewardImage.transform.DOScale(1f, 0.75f)
                       .SetEase(Ease.InCubic)
                       .Play().OnComplete(() =>
                       {
                           LevelView.CloseScreen(rewardScreen);
                           footerCanvas.sortingOrder = 11;
                           shopCanvas.sortingOrder = 10;
                           MainMenuView.FlyCoinsToLevelsChest(TimeHeistController.previousTimeHeistModel.rewardCount);
                           rewardImage.position = rewardIconStartPos;
                           rewardImage.DOScale(2.2f, 0);
                           rewardImage.DOScale(1.47f, 0.5f);

                           MainMenuView.coinsFly.OnCompletedSpawn += () => 
                           {
                               if (CurrencyController.TimeHeistDiamondsToAdd > 0) MainMenuView.UpdateViewWithMore20Diamonds?.Invoke();
                           };
                           GameController.SaveGameData();

                 
                       }));
                }, () =>
                {
                    UpdateRewardIcon();
                });
            }
            else if(TimeHeistController.previousTimeHeistModel.rewardType == "Hearts")
            {
                MakeImpuleAnimation(rewardImage, () =>
                {
                    LevelView.CloseScreen(rewardScreen);
                    var sequence = rewardImage.DOJump(targetPos, 0.25f, 1, 0.75f, false)
                       .Join(rewardImage.transform.DOScale(1f, 0.75f)
                       .SetEase(Ease.InCubic)
                       .Play().OnComplete(() =>
                       {
                           LevelView.CloseScreen(rewardScreen);
                           footerCanvas.sortingOrder = 11;
                           shopCanvas.sortingOrder = 10;
                           MainMenuView.FlyHeartToLevelsChest(TimeHeistController.previousTimeHeistModel.rewardCount);
                           HeartsController.Instance.SwitchToUnlimited(TimeHeistController.previousTimeHeistModel.rewardCount * 60);
           
                           rewardImage.position = rewardIconStartPos;
                           rewardImage.DOScale(2.2f, 0);
                           rewardImage.DOScale(1.47f, 0.5f);

                           GameController.SaveGameData();

                           MainMenuView.heartsFly.OnCompleted += () =>
                           {
                               if (CurrencyController.TimeHeistDiamondsToAdd > 0) MainMenuView.UpdateViewWithMore20Diamonds?.Invoke();
                           };


                       }));
                }, () =>
                {
                    UpdateRewardIcon();
                });
            }
            else if (TimeHeistController.previousTimeHeistModel.rewardType == "Boosters")
            {
                CurrencyController.Hints += TimeHeistController.previousTimeHeistModel.rewardCount;
                CurrencyController.Fans += TimeHeistController.previousTimeHeistModel.rewardCount;

                hintRewardTextCount.text = TimeHeistController.previousTimeHeistModel.rewardCount.ToString();
                fanRewardTextCount.text = TimeHeistController.previousTimeHeistModel.rewardCount.ToString();

                giftImage.sprite = giftOpenedSprite;

                hintRewardRect.position = rewardImage.position;
                fanRewardRect.position = rewardImage.position;

                hintRewardRect.gameObject.SetActive(true);
                fanRewardRect.gameObject.SetActive(true);

                hintRewardRect.DOScale(0.1f, 0);
                fanRewardRect.DOScale(0.1f, 0);

                var hintSequence = hintRewardRect.DOJump(hintRewardTargetRect.position, 0.25f, 1, 0.75f, false)
               .Join(hintRewardRect.transform.DOScale(1f, 0.75f)
               .SetEase(Ease.InCubic)
               .Play());

                var boosterSequence = fanRewardRect.DOJump(fanRewardTargetRect.position, 0.25f, 1, 0.75f, false)
             .Join(fanRewardRect.transform.DOScale(1f, 0.75f)
             .SetEase(Ease.InCubic)
             .Play()).OnComplete(() =>
             {
                 LevelView.CloseScreen(rewardScreen);

                 MakeImpuleAnimation(rewardImage, () =>
                 {
                     LevelView.CloseScreen(rewardScreen);
                     footerCanvas.sortingOrder = 11;
                     shopCanvas.sortingOrder = 10;
                     var sequence = rewardImage.DOJump(targetPos, 0.25f, 1, 0.75f, false)
                        .Join(rewardImage.transform.DOScale(1f, 0.75f)
                        .SetEase(Ease.InCubic)
                        .Play().OnComplete(() =>
                        {
                            rewardImage.position = rewardIconStartPos;
                            rewardImage.DOScale(2.2f, 0);
                            rewardImage.DOScale(1.47f, 0.5f);
                            giftImage.sprite = giftClosedSprite;
                            GameController.SaveGameData();


                            if (CurrencyController.TimeHeistDiamondsToAdd > 0) MainMenuView.UpdateViewWithMore20Diamonds?.Invoke();


                            isGivingReward = false;
                        }));
                 }, () =>
                 {
                     UpdateRewardIcon();
                 });

                 var hintSequence = hintRewardRect.DOJump(playButtonRect.position, 0.25f, 1, 0.75f, false)
          .Join(hintRewardRect.transform.DOScale(0.5f, 0.75f)
          .SetEase(Ease.InCubic)
          .Play());

                 var boosterSequence = fanRewardRect.DOJump(playButtonRect.position, 0.25f, 1, 0.75f, false)
              .Join(fanRewardRect.transform.DOScale(0.5f, 0.75f)
              .SetEase(Ease.InCubic)
              .Play()).OnComplete(() =>
              {

                  MakeImpuleAnimation(playButtonRect);
                  fanRewardRect.gameObject.SetActive(false);
                  hintRewardRect.gameObject.SetActive(false);
              });
             });
            }
        }
        private void MakeImpuleAnimation(Transform transform, Action OnComplete = null, Action OnMiddle = null)
        {
            Vector3 startScale = transform.localScale;
            transform.DOScale(startScale * 1.5f, 0.15f).OnComplete(() =>
            {
                OnMiddle?.Invoke();
                transform.DOScale(startScale, 0.25f).OnComplete(() => { OnComplete?.Invoke(); });

            });
        }
    }
}