using UnityEngine;
using UnityEngine.UI;
using System;

namespace Matchmania
{
    public class ReviveView : MonoBehaviour
    {
        [SerializeField] private SFXConfig SFXConfig;
        [SerializeField] private MainMenuView mainMenu;

        [Header("Buttons")]
        [SerializeField] private Button reviveAdsButton;
        [SerializeField] private Button reviveCoinsButton;
        [SerializeField] private Button rewardedVideoForCoinsButton;

        [Header("Tabs")]
        [SerializeField] private GameObject coinsReviveTab;
        [SerializeField] private GameObject adsReviveTab;

        public static string savedDateTime;
        DateTime DateTime;

        private void Start()
        {
            reviveAdsButton.onClick.AddListener(LevelView.Instance.OnContiniueLevelAddButtonPressed);
            reviveAdsButton.onClick.AddListener(ReviveAdsButton);
            reviveCoinsButton.onClick.AddListener(LevelView.Instance.OnContiniueLevelHardCurrencyButtonPressed);
            rewardedVideoForCoinsButton.onClick.AddListener(RewardedVideoForCoinsButtonPressed);
        }
        private void OnDisable()
        {
            if(LevelView.Instance != null) LevelView.Instance.mainMenuHeader.SetActive(false);
            AdvertisementSystem.ShowBanner();
        }
        private void RewardedVideoForCoinsButtonPressed()
        {
            bool available = AdvertisementSystem.IsRewardedAdReady();

            if (available)
            {
                LevelView.Instance.loadingScreen.SetActive(true);
                LevelController.Instance.IsPlayingLevel = false;
                AdvertisementSystem.ShowRewardedAd();

                SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
     
                AdvertisementSystem.OnEndRewardVideo += () => {

                    CurrencyController.Coins += 100;
                    CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
                    mainMenu.UpdateCoinsInWallet();
                    GameController.SaveGameData();
                };
            }
            else
            {
               LevelView.Instance.noAvailableVideo.SetActive(true);
            }
        }
        private void ReviveAdsButton()
        {
            savedDateTime = DateTime.Now.ToString();
            GameController.SaveGameData();
        }
        private void OnEnable()
        {
            if(savedDateTime == "" || string.IsNullOrEmpty(savedDateTime))
            {
                adsReviveTab.SetActive(true);
                coinsReviveTab.SetActive(false);
                rewardedVideoForCoinsButton.gameObject.SetActive(false);
            }
            else
            {
                DateTime.TryParse(savedDateTime, out DateTime);

               TimeSpan timeSpan = DateTime.Now - DateTime;

                if(timeSpan.Days > 0)
                {
                    adsReviveTab.SetActive(true);
                    coinsReviveTab.SetActive(false);
                    rewardedVideoForCoinsButton.gameObject.SetActive(false);
                }
                else
                {
                    adsReviveTab.SetActive(false);
                    coinsReviveTab.SetActive(true);

                    LevelView.Instance.mainMenuHeader.SetActive(true);
                    AdvertisementSystem.HideBanner();

                    rewardedVideoForCoinsButton.gameObject.SetActive(true);
                }
            }
        }
    }
}
