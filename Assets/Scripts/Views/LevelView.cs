﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;
using HyperCasualTemplate;
using System.Collections;
using DG.Tweening;
using Dreamteck.Splines;
using MoreMountains.NiceVibrations;
using com.adjust.sdk;
using Lean.Pool;

namespace Matchmania
{
    public class LevelView : MonoBehaviour
    {
        #region Singleton
        private static LevelView _instance;
        public static LevelView Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<LevelView>();
                }

                return _instance;
            }
        }
        #endregion

        public static  Action GoHomeEventHandler;
        public static Action ToMainMenuEventHandler;
        public static event Action UpdateLevelIndexTexts;

        [SerializeField] private VFXConfig VFXConfig;
        [SerializeField] private Transform starEffectParent;
        [SerializeField] private SplineComputer starEffectSpline;
        [SerializeField] public MainMenuView mainMenuView;
        [SerializeField] public FooterView FooterView;
        [SerializeField] private Renderer[] slotSprites;

        [SerializeField] private Transform obstacleParent;
        [SerializeField] private Transform[] tutorialItemsPositions;
        [SerializeField] private GameObject slotsParent;
        [SerializeField] private GameObject mainMenuScreen;
        [SerializeField] private GameObject levelScreen;
        [SerializeField] private GameObject shopScreen;
        [SerializeField] private GameObject removeAdsScreen;
        [SerializeField] private GameObject OnPurchaseFailScreen;
        [SerializeField] private Transform itemsParent;
        [SerializeField] private GameObject hintsButtons;
        [SerializeField] private SFXConfig SFXConfig;
        public GameObject loadingScreen;
        [SerializeField] private Slider comboBonusSlider;
        [SerializeField] private Image timerClockImage;
        [SerializeField] private ParticleSystem confeti;

        [SerializeField] private GameObject diomandsWallet;
        [SerializeField] private GameObject starsWallet;

        public Transform itemStartPosition;

        [Header("Texts")]
        [SerializeField] private TextMeshProUGUI bonusStarsText;
        [SerializeField] private TextMeshProUGUI timerText;
        [SerializeField] private TextMeshProUGUI levelStarText;
        [SerializeField] private TextMeshProUGUI levelDiomandsText;
        [SerializeField] private TextMeshProUGUI levelIndexText;
        [SerializeField] private TextMeshProUGUI finishLevelStarsCountText;
        [SerializeField] private TextMeshProUGUI finishLevelIndexText;
        [SerializeField] private TextMeshProUGUI hintsCountText;
        [SerializeField] private TextMeshProUGUI fansCountText;
        [SerializeField] private TextMeshProUGUI afterFinishLevelStartCountText;
        [SerializeField] private TextMeshProUGUI comboBonusSliderText;
        [SerializeField] private TextMeshProUGUI levelChestSliderText;
        [SerializeField] private TextMeshProUGUI starChestSliderText;
        [SerializeField] private TextMeshProUGUI[] coinHoldersTexts;

        [Header("Sliders")]

        [SerializeField] private Slider levelChestSlider;
        [SerializeField] private Slider starChestSlider;

        [Header("Screens")]
        public GameObject victoryScreen;
        public GameObject mainMenuHeader;
        public GameObject afterVictoryScreen;
        public GameObject fakeLeaderboardScreen;
        [SerializeField] private GameObject defeatScreenOOF;
        [SerializeField] private GameObject defeatScreenTU;
        [SerializeField] private GameObject afterDefeatScreen;
        public GameObject noAvailableVideo;
        //[SerializeField] private GameObject levelsMap;
        //[SerializeField] private GameObject levelsMapPrefab;
        [SerializeField] private GameObject fanShop;
        [SerializeField] private GameObject hintShop;

        [Header("Defeat Screen")]

        [SerializeField] private TextMeshProUGUI reviveCostText;
        [SerializeField] private TextMeshProUGUI reviveCostTimeOutText;



        [Header("Buttons")]
        [SerializeField] private Button nextLevelButton;
        [SerializeField] private Button restartLevelButton;
        [SerializeField] private Button pauseHomeButton;
        [SerializeField] private Button defeatHomeButton;
        [SerializeField] private Button hintButton;
        [SerializeField] private Button fanButton;
        [SerializeField] private Button closeDefeatScreenButton;
        [SerializeField] private Button closeTUDefeatScreenButton;
        [SerializeField] private Button closeOOBDefeatScreenButton;
        [SerializeField] private Button finishLevelClaimRewardButton;
        [SerializeField] private Button finishLevelClaimBonusRewardButton;
        [SerializeField] private Button pauseButton;
        [SerializeField] private Button shopOpenButton;
        [SerializeField] private Button shopCloseButton;
        [SerializeField] private Button continiueLevelHardCurrencyButtonTIO;
        [SerializeField] private Button continiueLevelHardCurrencyButtonOOB;
        [SerializeField] private Button continiueLevelAddButtonOOB;
        [SerializeField] private Button starChestButton;
        [SerializeField] private Button levelChestButton;
        [SerializeField] private Button closeBuyHintsShop;
        [SerializeField] private Button closeBuyFansShop;
        [SerializeField] private Button closeRemoveAdsDialog;

        private IncrementalReviveCostController _incrementalReviveCostController;

        public static bool isPlayingTimerSound;
        public static bool isComeFormInterstitial;
        private bool isPlayingVictory;
        private bool isDefeat;
        [SerializeField] private Obstacle rotationBeam;
        private Obstacle currentObstacle;
        private float timerSpeed;

        private void Start()
        {
            LevelController.StartLevelEventHandler += StartLevel;
            LevelController.Instance.DefeatEventHandler += OnDefeat;
            LevelController.Instance.VictoryEventHandler += OnVictory;
            LevelController.Instance.MarchEventHandler += UpdateLevelStarsOrDiomands;
            HintController.Instance.HintUsedEventHandler += UpdateHintsCount;
            FanController.Instance.FanUsedEventHandler += UpdateHintsCount;
            IAPController.OnPurchaseEventHandler += CloaseLoadingScreen;
            AdvertisementSystem.OnFinishVideo += CloaseLoadingScreen;
            //PlayerLevelUpgrade.OnLevelUpgradeEventHandler += OpenPlayerUpgradeScreen;

            hintButton.onClick.AddListener(HintButtonPressed);
            fanButton.onClick.AddListener(FanButtonPressed);
            //nextLevelButton.onClick.AddListener(NextLevelButtonPressed);
            restartLevelButton.onClick.AddListener(RestartLevelButtonPressed);
            finishLevelClaimRewardButton.onClick.AddListener(OnFinishLevelClaimRewardButtonPressed);
            finishLevelClaimBonusRewardButton.onClick.AddListener(OnFinishLevelClaimBonusRewardButtonPressed);
            defeatHomeButton.onClick.AddListener(OnHomeButtonPressed);
            //closeDefeatScreenButton.onClick.AddListener(CloseDefeatScreenButtonPressed);
            pauseButton.onClick.AddListener(PauseButtonPressed);
            shopCloseButton.onClick.AddListener(CloseShop);
            closeTUDefeatScreenButton.onClick.AddListener(CloseDefeatScreenButtonPressed);
            closeOOBDefeatScreenButton.onClick.AddListener(CloseDefeatScreenButtonPressed);
            //starChestButton.onClick.AddListener(mainMenuView.StarsChestRewardButtonPressed);
            //levelChestButton.onClick.AddListener(mainMenuView.LevelsChestRewardButtonPressed);
            //starChestButton.onClick.AddListener(UpdateFinishLevelChestsView);
            //levelChestButton.onClick.AddListener(UpdateFinishLevelChestsView);
            //continiueLevelHardCurrencyButtonTIO.onClick.AddListener(OnContiniueLevelHardCurrencyButtonPressed);
            //continiueLevelHardCurrencyButtonOOB.onClick.AddListener(OnContiniueLevelHardCurrencyButtonPressed);
            //continiueLevelAddButtonOOB.onClick.AddListener(OnContiniueLevelAddButtonPressed);
            closeRemoveAdsDialog.onClick.AddListener(CloseRemoveAdsButtomPressed);
            closeBuyHintsShop.onClick.AddListener(CloseBuyHintShop);
            closeBuyFansShop.onClick.AddListener(CloseBuyHintShop);
            
            shopOpenButton.onClick.AddListener(OpenShop);

            //starChestButton.onClick.AddListener(mainMenuView.OpenStarsChestRewardScreen);
            //levelChestButton.onClick.AddListener(mainMenuView.OpenLevelsChestRewardScreen);
            IAPController.OnPurchaseEventHandler += UpdateAllCoinsHolderAmounts;

            ToMainMenuEventHandler += ToMainMenu;

            UpdateAllCoinsHolderAmounts();

            _incrementalReviveCostController = new IncrementalReviveCostController();

            UpdateRewardedChestsView();

            //foreach (var renderer in slotSprites)
            //{
            //    renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
            //    renderer.receiveShadows = true;
            //}
        }
  private void ToMainMenu()
        {
            LevelController.Instance.FadeEffect(0, OnGetMainMenu);
        }
        private void Update()
        {
            if (LevelController.Timer > 0)
            {
                DisplayTime(LevelController.Timer);
            }
            if (LevelController.Timer < 10 && LevelController.Timer > 0)
            {
                if (LevelController.Instance.IsPlayingLevel && isPlayingTimerSound == false)
                {
                    SFXConfig.PlaySoundEffect(SFXConfig.timerRemaining);
                    isPlayingTimerSound = true;
                }
            }

            comboBonusSlider.value = LevelController.Instance.comboBonusTimer;
            comboBonusSlider.maxValue = LevelController.Instance.comboBonusTimerMax;
            comboBonusSliderText.text = "x" + LevelController.Instance.comboBonusIndex;

        }
        public void OpenRemoveAdsDialog()
        {
            OpenScreen(removeAdsScreen);
        }
        private void CloseRemoveAdsButtomPressed()
        {
            CloseScreen(removeAdsScreen);
        }
        public void OpenShop()
        {
            if(FooterView.currentScreenType == FooterView.CurrentScreenType.level)
            {
                OpenScreen(shopScreen);
                mainMenuView.UpdateMainMenuView();
                RectTransform shopRectTransform = shopScreen.GetComponent<RectTransform>();
                shopRectTransform.localPosition = new Vector3(0, shopRectTransform.localPosition.y, 0);
                mainMenuHeader.SetActive(false);
            }
            else
            {
                if (FooterView.isCanSwipe == true)
                {
                    FooterView.ShopButtonPressed();
                }
            }
            if (LevelController.Timer > 0)
            {
                LevelController.Instance.IsPlayingLevel = false;
                LevelController.Instance.IsCanMakeTurn = false;

            }

        }
        private void CloaseLoadingScreen()
        {
            loadingScreen.SetActive(false);
        }
        public void OnPurchaseFail()
        {
            OnPurchaseFailScreen.SetActive(true);
            AdvertisementSystem.isCanShowInactiveAdd = true;
            CloaseLoadingScreen();
        }

        private void CloseShop()
        {
            CloseScreen(shopScreen);
            if (FooterView.isCanSwipe == true && FooterView.currentScreenType != FooterView.CurrentScreenType.level)
            {
                FooterView.HomeButtonPressed();
            }
            mainMenuHeader.SetActive(true);
        }
        private void PauseButtonPressed()
        {
            PauseController.PauseGame(true);
        }
        private void DisplayTime(float timeToDisplay)
        {
            float minutes = Mathf.FloorToInt(timeToDisplay / 60);
            float seconds = Mathf.FloorToInt(timeToDisplay % 60);
            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
        public void StartLevel()
        {
            slotsParent.SetActive(true);


            FooterView.isCanSwipe = false;


            ItemsSpawnController.Instance.SpawnItems();
          

            LevelController.Timer = LevelController.Instance.CurrentLevelModel.timer;

            isPlayingTimerSound = false;
            LevelController.Instance.CurrentLevelStars = 0;
            levelStarText.text = LevelController.Instance.CurrentLevelStars.ToString();
            levelIndexText.text = "Level " + (LevelController.CurrentLevelIndex + 1).ToString();
            UpdateHintsCount();
            hintsButtons.SetActive(true);
            mainMenuHeader.SetActive(false);
            LeaveConfirmationView.Instance.HideConfirmation();
            _incrementalReviveCostController.ResetPrice();

            diomandsWallet.SetActive(false);
            starsWallet.SetActive(true);
        }
        private void OnVictory()
        {

            SFXConfig.PlayVictorySFX(this);
            AudioController.Instance.PlayMusic(false);

            if (SettingsController.isVibrationOn == true)
                MMVibrationManager.Haptic(HapticTypes.Success, false, true, this);

            if (!isPlayingVictory)
                StartCoroutine(TurnOnVIctoryScreen());
        }
        IEnumerator TurnOnVIctoryScreen()
        {
            isPlayingVictory = true;
            UpdateAllCoinsHolderAmounts();
            RectTransform targetPos = GameObject.FindGameObjectWithTag("Star").GetComponent<RectTransform>();
            Vector3 splinePointPos = targetPos.position;
            splinePointPos.z = starEffectSpline.GetPointPosition(0).z;
            starEffectSpline.SetPointPosition(2, splinePointPos);

            yield return new WaitForSeconds(1.5f);

            if (TimeHeistController.isTimeHeistEventEnabled)
            {
                starsWallet.SetActive(false);
                diomandsWallet.SetActive(true);
            }
            else
            {
                starsWallet.SetActive(true);
                diomandsWallet.SetActive(false);
            }
             timerSpeed = 5;


            StartCoroutine(SpawnDiomands());

            float value = LevelController.Timer;

            float to = 0;
            var ds = DOTween.To(() => value, x => value = x, to, 4).OnUpdate(() =>
            {

                LevelController.Timer = value;
            });
       
            yield return new WaitUntil(() => LevelController.Timer <= 0);
            //OpenScreen(afterVictoryScreen);
            FakeLeaderboardController.OpenAtLevelEndEventHandler?.Invoke();
            mainMenuHeader.SetActive(false);


            CurrencyController.LevelsChestCount++;

            afterFinishLevelStartCountText.text = LevelController.Instance.CurrentLevelStars.ToString();
            levelStarText.text = LevelController.Instance.CurrentLevelStars.ToString();
            bonusStarsText.text = "+" + (LevelController.Instance.CurrentLevelStars * 2).ToString();
            hintsButtons.SetActive(false);

            yield return new WaitForSeconds(1f);
            finishLevelClaimRewardButton.gameObject.SetActive(true);
            finishLevelClaimRewardButton.GetComponent<CanvasGroup>().alpha = 0;
            finishLevelClaimRewardButton.GetComponent<CanvasGroup>().DOFade(1f, 1f);

            if (LevelController.CurrentLevelIndex == 10)
            {
                AnalyticsEvents.ExecuteAdjustEvent("ift5y8");
            }
            else if (LevelController.CurrentLevelIndex == 50)
            {
                AnalyticsEvents.ExecuteAdjustEvent("pm8y3v");
            }
            else if (LevelController.CurrentLevelIndex == 100)
            {
                AnalyticsEvents.ExecuteAdjustEvent("lamjue");
            }
            isPlayingVictory = false;
            LevelController.CurrentLevelIndex++;
            GameController.SaveGameData();

        }

        private IEnumerator SpawnDiomands()
        {
            int count = GetDiomandsRewardCount();
            int diamondsToSpawn = 0;
            float spawnInterval = 0.5f;

            if(count <= 10)
            {
                diamondsToSpawn = count;
            }
            else
            {
                diamondsToSpawn = 10;
            }
            float value = LevelController.currentLevelDiamonds;

            float to = value + count;
            var ds = DOTween.To(() => (int)value, x => value = x, (int)to, 4).OnUpdate(() =>
            {

                if (TimeHeistController.isTimeHeistEventEnabled)
                {
                    LevelController.currentLevelDiamonds = (int)value;
                }
                else
                {
                    LevelController.Instance.CurrentLevelStars = (int)value;
                }
                LevelView.Instance.UpdateLevelStarsOrDiomands();

            });

            CurrencyController.TimeHeistDiamondsToAdd += (int)to;
            LevelController.currentLevelDiamonds = 0;

            for (int i = 0; i < diamondsToSpawn; i++)
            {
                GameObject starEffect = null;
                if (TimeHeistController.isTimeHeistEventEnabled)
                {
                    starEffect = VFXConfig.PlayEffect(Vector3.zero, VFXConfig.diamondFromTimer, Vector3.zero);
                    starEffect.GetComponent<StarUIEffect>().targetTransform = diomandsWallet.transform;
                }
                else
                {
                    starEffect = VFXConfig.PlayEffect(Vector3.zero, VFXConfig.starFromTimer, Vector3.zero);
                    starEffect.GetComponent<StarUIEffect>().targetTransform = starsWallet.transform;
                }
                starEffect.GetComponent<SplineFollower>().spline = starEffectSpline;

                RectTransform rect = starEffect.GetComponent<RectTransform>();
                rect.transform.SetParent(starEffectParent);
                rect.transform.localScale = Vector3.one;

                rect.DOScale(2, 0.1f).OnComplete (() => { rect.DOScale(1, 1f); });

                yield return new WaitForSeconds(spawnInterval);

                spawnInterval -= spawnInterval * 0.2f;

                if(spawnInterval < 0.1f)
                {
                    spawnInterval = 0.1f;
                }
  
            }
            confeti.Play();

            yield return new WaitForSeconds(1.5f);

            confeti.Stop();
        }
        private int GetDiomandsRewardCount()
        {
            int diomandsCount = 0;

            for (int i = 0; i < GameModel.DiomandsGains.DiomandsGains.Length; i++)
            {
               if(LevelController.Timer >= GameModel.DiomandsGains.DiomandsGains[i].time)
                {
                    diomandsCount = GameModel.DiomandsGains.DiomandsGains[i].total;
                }
            }

            return diomandsCount;
        }
        public void UpdateRewardedChestsView()
        {
            //levelChestSlider.value = CurrencyController.LevelsChestCount;
            //starChestSlider.value = CurrencyController.StarsChestCount;
            //UpdateFinishLevelChestsView();
        }
        private void UpdateFinishLevelChestsView()
        {
            if (CurrencyController.LevelsChestCount >= levelChestSlider.maxValue)
            {
                levelChestSliderText.text = "Tap to Claim";
            }
            else levelChestSliderText.text = Mathf.RoundToInt(levelChestSlider.value).ToString() + "/" + levelChestSlider.maxValue.ToString();

            if (CurrencyController.StarsChestCount >= starChestSlider.maxValue)
            {
                starChestSliderText.text = "Tap to Claim";
            }
            else starChestSliderText.text = Mathf.RoundToInt(starChestSlider.value).ToString() + "/" + starChestSlider.maxValue.ToString();
        }
        IEnumerator _OnDefeat(bool isOutOfBox)
        {
            isDefeat = true;

            reviveCostText.text = _incrementalReviveCostController.CurrentPrice.ToString();
            reviveCostTimeOutText.text = _incrementalReviveCostController.CurrentPrice.ToString();

            if (isOutOfBox)
            {
                OpenScreen(defeatScreenOOF);
            }
            else
            {
                OpenScreen(defeatScreenTU);
            }
            UpdateAllCoinsHolderAmounts();
            LevelController.Instance.isOutOfBox = isOutOfBox;
            mainMenuHeader.SetActive(true);
            SFXConfig.PlaySoundEffect(SFXConfig.defeat);
            AudioController.Instance.PlayMusic(false);
            LevelController.Instance.IsPlayingLevel = false;

            if (SettingsController.isVibrationOn == true)
                MMVibrationManager.Haptic(HapticTypes.Failure, false, true, this);
            hintsButtons.SetActive(false);

            AnalyticsEvents.ExecuteEvent("Level_Failed_" + LevelController.CurrentLevelIndex);

            yield return new WaitForSeconds(0.1f);
            isDefeat = false;
        }
        private void OnDefeat(bool isOutOfBox)
        {
            if (!isDefeat)
                StartCoroutine(_OnDefeat(isOutOfBox));

            LevelController.Instance.isReviveScreenOpened = true;
            UpdateLevelIndexTexts?.Invoke();
        }
        public void RestartLevelButtonPressed()
        {
            if (HeartsController.Instance.HeartsCheck() == false) return;

            HeartsController.Instance.CurrentHeartsAmount--;


            var startLevel = Convert.ToInt32(FirebaseInitializer.defaults["StartLevel"]);
            var levelCooldown = Convert.ToInt32(FirebaseInitializer.defaults["LevelCooldown"]);

            if (LevelController.CurrentLevelIndex % levelCooldown == 0 && LevelController.CurrentLevelIndex >= startLevel)
            {
                if (AdvertisementSystem.IsInterstitialdAdReady() && !AdvertisementSystem.isAdsRemoved && AdvertisementSystem.isCanShowInterstital)
                {
                    AdvertisementSystem.ShowInterstitial();
                    LevelController.isRestartLevelAfterInterstitional = true;
                    LevelController.Instance.IsPlayingLevel = false;

                }
                else
                {
                    LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
                }
            }
            else
            {
                LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
            }
            CloseScreen(victoryScreen);
            CloseScreen(defeatScreenTU);
            CloseScreen(defeatScreenOOF);
            CloseScreen(afterDefeatScreen);
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            mainMenuHeader.SetActive(false);
        }
        private void HintButtonPressed()
        {
            if (CurrencyController.Hints <= 0)
            {
                OpenHintBoosterShop();
                return;
            }
            HintController.Instance.UseHint();
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
        }
        public void NextLevelButtonPressed()
        {
            victoryScreen.SetActive(false);
            afterVictoryScreen.SetActive(false);
            defeatScreenOOF.SetActive(false);
            defeatScreenTU.SetActive(false);
            slotsParent.gameObject.SetActive(false);
            hintsButtons.SetActive(false);


            mainMenuScreen.SetActive(false);
            levelScreen.SetActive(true);
            mainMenuHeader.SetActive(false);

            PlayerLevelUpgrade.OnUpdateEventHandler?.Invoke();
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);

            var startLevel = Convert.ToInt32(FirebaseInitializer.defaults["StartLevel"]);
            var levelCooldown = Convert.ToInt32(FirebaseInitializer.defaults["LevelCooldown"]);


            if (LevelController.CurrentLevelIndex >= startLevel && LevelController.CurrentLevelIndex % levelCooldown == 0)
            {
                if (IronSource.Agent.isInterstitialReady() && AdvertisementSystem.isAdsRemoved == false && AdvertisementSystem.isCanShowInterstital == true)
                {
                    AdvertisementSystem.isNextLevelAfterInterstitial = true;
                    AdvertisementSystem.ShowInterstitial();

                    if (AdvertisementSystem.isBannerActive == false)
                    {
                        AdvertisementSystem.ShowBanner();
                    }
                }
                else LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
            }
            else LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
        }
        public void UpdateLevelStarsOrDiomands()
        {
            levelStarText.text = LevelController.Instance.CurrentLevelStars.ToString();
            levelDiomandsText.text = LevelController.currentLevelDiamonds.ToString();
            //SFXConfig.PlaySoundEffect(SFXConfig.matchItems);
        }
        public void OnFinishLevelClaimRewardButtonPressed()
        {
            CloseScreen(afterVictoryScreen);
            CloseScreen(fakeLeaderboardScreen);
            OpenScreen(victoryScreen);
            mainMenuHeader.SetActive(false);

            FinishLevel();

            //float time = 1f;

            //float levelChestValue = levelChestSlider.value + 1;
            //float starChestValue = starChestSlider.value + LevelController.Instance.CurrentLevelStars;

            //DOTween.To(() => levelChestSlider.value, x => levelChestSlider.value = x, levelChestValue, time).OnUpdate(UpdateFinishLevelChestsView).OnComplete(UpdateRewardedChestsView);
            //DOTween.To(() => starChestSlider.value, x => starChestSlider.value = x, starChestValue, time).OnUpdate(UpdateFinishLevelChestsView).OnComplete(UpdateRewardedChestsView);

            //var startLevel = Convert.ToInt32(FirebaseInitializer.defaults["StartLevel"]);
            //var levelCooldown = Convert.ToInt32(FirebaseInitializer.defaults["LevelCooldown"]);

            //if (LevelController.CurrentLevelIndex % 11 == 0 && LevelController.CurrentLevelIndex != 0)
            //{
            //    RateUsView.Instance.ShowRateUsScreen();
            //}
            //else if (LevelController.CurrentLevelIndex >= startLevel && LevelController.CurrentLevelIndex % levelCooldown == 0)
            //{
            //    if (AdvertisementSystem.IsInterstitialdAdReady())
            //    {
            //        AdvertisementSystem.ShowInterstitial();
            //        if (AdvertisementSystem.isBannerActive == false)
            //        {
            //            AdvertisementSystem.ShowBanner();
            //        }
            //    }
            //}
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            PlayerLevelUpgrade.OnUpdateEventHandler?.Invoke();
            //CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            finishLevelClaimRewardButton.gameObject.SetActive(false);
        }
        public void OnFinishLevelClaimBonusRewardButtonPressed()
        {
            bool available = AdvertisementSystem.IsRewardedAdReady();
            mainMenuHeader.SetActive(false);
            if (available)
            {
                loadingScreen.SetActive(true);
                AdvertisementSystem.ShowRewardedAd();

                CloseScreen(afterVictoryScreen);
                CloseScreen(fakeLeaderboardScreen);
                OpenScreen(victoryScreen);
                mainMenuHeader.SetActive(true);
                LevelController.Instance.CurrentLevelStars *= 3;

                //CurrencyController.Stars += LevelController.Instance.CurrentLevelStars;
                //CurrencyController.StarsChestCount += LevelController.Instance.CurrentLevelStars;

                PlayerLevelUpgrade.OnUpdateEventHandler?.Invoke();
                //CurrencyController.OnChangeCurrencyEventHandler?.Invoke();

                FinishLevel();

                GameController.SaveGameData();
                SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);

                finishLevelClaimRewardButton.gameObject.SetActive(false);
            }
            else
            {
                OpenScreen(noAvailableVideo);
            }
        }
        private void FinishLevel()
        {
            //finishLevelIndexText.text = "Level " + (LevelController.CurrentLevelIndex + 1).ToString();
            finishLevelStarsCountText.text = LevelController.Instance.CurrentLevelStars.ToString();
            //TTEventController.Instance.HandleMaxAmountOfCombo(LevelController.Instance.maxCombo);
            //TTEventController.Instance.ShowEndLevelBar();
            //TTEventController.Instance.UpdateOpenedMainMenu();
            if (LevelController.CurrentLevelIndex >= GameModel.Levels.Levels.Length)
            {
                LevelController.CurrentLevelIndex = GameModel.Levels.Levels.Length - 1;
            }

            CurrencyController.Stars += LevelController.Instance.CurrentLevelStars;
            CurrencyController.StarsChestCount += LevelController.Instance.CurrentLevelStars;

            GameController.SaveGameData();

            PlayerLevelUpgrade.UpgradeLevel();

            if (LevelController.CurrentLevelIndex % 10 == 0)
            {
                RateUsView.Instance.ShowRateUsScreen();
            }
        }
        public void GoToMainMenu()
        {
  
            LevelController.Instance.ClearLevel();
            LevelController.Instance.IsPlayingLevel = false;
            LevelController.Instance.isReviveScreenOpened = false;
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            PlayerLevelUpgrade.OnUpdateEventHandler?.Invoke();
            AdvertisementSystem.HideBanner();

            var startLevel = Convert.ToInt32(FirebaseInitializer.defaults["StartLevel"]);
            var levelCooldown = Convert.ToInt32(FirebaseInitializer.defaults["LevelCooldown"]);


            if (LevelController.CurrentLevelIndex >= startLevel && LevelController.CurrentLevelIndex % levelCooldown == 0)
            {
                if (AdvertisementSystem.IsInterstitialdAdReady() && AdvertisementSystem.isAdsRemoved == false && AdvertisementSystem.isCanShowInterstital == true)
                {
                    AdvertisementSystem.ShowInterstitial();
                    AdvertisementSystem.isMainMenuAfterInterstitial = true;
                    return;
                }
            }

            LevelController.Instance.FadeEffect(0, OnGetMainMenu);
        }
        public void OnGetMainMenu()
        {

            //TTEventController.Instance.ShowTheBar();
            mainMenuScreen.SetActive(true);

            victoryScreen.SetActive(false);
            afterVictoryScreen.SetActive(false);
            afterDefeatScreen.SetActive(false);
            defeatScreenTU.SetActive(false);
            defeatScreenOOF.SetActive(false);
            slotsParent.gameObject.SetActive(false);
            hintsButtons.SetActive(false);
            mainMenuScreen.SetActive(true);
            mainMenuHeader.SetActive(true);
            levelScreen.SetActive(false);
            MainMenuView.Instance.FlyStarsToLevelsChest();
            GoHomeEventHandler?.Invoke();
            FooterView.isCanSwipe = true;
            FooterView.currentScreenType = FooterView.CurrentScreenType.home;
            mainMenuView.footerScreen.SetActive(true);
        }
        public void OnHomeButtonPressed()
        {
            GoToMainMenu();
            HeartsController.Instance.CurrentHeartsAmount--;
            LevelController.Instance.CurrentLevelStars = 0;
        }
        private void CloseDefeatScreenButtonPressed()
        {
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            if (LeaveConfirmationView.Instance.ShowConfirmation() == false) return;
            defeatScreenOOF.SetActive(false);
            defeatScreenTU.SetActive(false);
            afterDefeatScreen.SetActive(true);
            afterDefeatScreen.transform.localScale = Vector3.one;
            afterDefeatScreen.transform.GetChild(0).GetComponent<Image>().DOFade(0.9f, 0);
        }
        public void OnContiniueLevelHardCurrencyButtonPressed()
        {
            if (CurrencyController.Coins >= _incrementalReviveCostController.CurrentPrice)
            {
                HintController.Instance.ContiniueLevelAfterDefeat();
                CloseScreen(defeatScreenOOF);
                CloseScreen(defeatScreenTU);
                SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
                AudioController.Instance.TurnOnMusicAL(true);
                CurrencyController.Coins -= _incrementalReviveCostController.CurrentPrice;
                hintsButtons.SetActive(true);
                LevelController.Instance.IsPlayingLevel = true;
                LeaveConfirmationView.Instance.HideConfirmation();
                _incrementalReviveCostController.IncreasePrice();
                mainMenuHeader.SetActive(false);
                GoHomeEventHandler?.Invoke();
                LevelController.Instance.isReviveScreenOpened = false;
            }
            else
            {
                OpenShop();
            }
        }
        public void OnContiniueLevelAddButtonPressed()
        {
            bool available = AdvertisementSystem.IsRewardedAdReady();

            if (available)
            {
                loadingScreen.SetActive(true);
                LevelController.Instance.IsPlayingLevel = false;
                AdvertisementSystem.ShowRewardedAd();
                AudioController.Instance.TurnOnMusicAL(false);
                SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
                mainMenuHeader.SetActive(false);

                AdvertisementSystem.OnEndRewardVideo += Revive;
                LevelController.Instance.isReviveScreenOpened = false;
            }
            else
            {
                noAvailableVideo.SetActive(true);
            }
        }
        public void Revive()
        {
            HintController.Instance.ContiniueLevelAfterDefeat();
            mainMenuHeader.SetActive(false);
            defeatScreenOOF.SetActive(false);
            defeatScreenTU.SetActive(false);
            hintsButtons.SetActive(true);
            LeaveConfirmationView.Instance.HideConfirmation();
            UpdateAllCoinsHolderAmounts();
        }
        private void UpdateHintsCount()
        {
            hintsCountText.text = CurrencyController.Hints.ToString();
            fansCountText.text = CurrencyController.Fans.ToString();

            if (CurrencyController.Hints <= 0)
            {
                hintsCountText.text = "+";
            }
            else
            {
                hintsCountText.text = CurrencyController.Hints.ToString();
            }
            if (CurrencyController.Fans <= 0)
            {
                fansCountText.text = "+";
            }
            else
            {
                fansCountText.text = CurrencyController.Fans.ToString();
            }
        }
        public void StartPurchusing()
        {
            AdvertisementSystem.isCanShowInactiveAdd = false;
        }

        private void OpenFanBoosterShop()
        {
            OpenScreen(fanShop);
            UpdateAllCoinsHolderAmounts();
            LevelController.Instance.IsCanMakeTurn = false;
            LevelController.Instance.IsPlayingLevel = false;
            mainMenuHeader.SetActive(true);
        }

        private void OpenHintBoosterShop()
        {
            OpenScreen(hintShop);
            UpdateAllCoinsHolderAmounts();
            LevelController.Instance.IsCanMakeTurn = false;
            LevelController.Instance.IsPlayingLevel = false;
            mainMenuHeader.SetActive(true);
        }
        private void CloseBuyHintShop()
        {
            CloseScreen(hintShop);
            CloseScreen(fanShop);
            mainMenuHeader.SetActive(false);

        }
        public void UpdateAllCoinsHolderAmounts()
        {
            foreach (var coinHolderText in coinHoldersTexts)
            {
                coinHolderText.text = CurrencyController.Coins.ToString();
            }
        }
        public void SetCanMakeTurn()
        {
            StartCoroutine(_SetCanMakeTurn());
        }
        IEnumerator _SetCanMakeTurn()
        {
            yield return new WaitForSeconds(.1f);
            LevelController.Instance.IsCanMakeTurn = true;
            LevelController.Instance.IsPlayingLevel = true;
        }
        public void BuyFanFromShop()
        {
            //if (LevelController.Instance.IsPlayingLevel == false) return;

            if (CurrencyController.Coins >= 90)
            {
                CurrencyController.Coins -= 90;
                CurrencyController.Fans += 3;
                UpdateHintsCount();
                CloseScreen(fanShop);
                HintController.Instance.CheckBoostersIcons();
                SetCanMakeTurn();
                mainMenuView.UpdateMainMenuView();
                mainMenuHeader.SetActive(false);

            }
            else
            {
                OpenShop();
            }

        }

        public void BuyHintFromShop()
        {
            //if (LevelController.Instance.IsPlayingLevel == false) return;

            if (CurrencyController.Coins >= 90)
            {
                CurrencyController.Coins -= 90;
                CurrencyController.Hints += 3;
                UpdateHintsCount();
                CloseScreen(hintShop);
                HintController.Instance.CheckBoostersIcons();
                mainMenuView.UpdateMainMenuView();
                SetCanMakeTurn();
                mainMenuHeader.SetActive(false);
            }
            else
            {
                OpenShop();
            }
        }
        private void FanButtonPressed()
        {
            if (CurrencyController.Fans <= 0)
            {
                OpenFanBoosterShop();
                return;
            }
            FanController.Instance.UseFan();
        }
        public static void OpenScreen(GameObject screen, float targetFade = 0.9f)
        {
            screen.SetActive(true);

            Image imageBG = screen.transform.GetChild(0).GetComponent<Image>();
            imageBG.DOFade(0f, 0f);

            screen.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

            if (!screen.GetComponent<CanvasGroup>())
            {
                screen.AddComponent<CanvasGroup>();
            }

            screen.GetComponent<CanvasGroup>().alpha = 0;
            screen.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
            var ease = screen.transform.DOScale(1, .25f);
            ease.SetEase(Ease.InCubic);
            ease.onComplete += () => imageBG.DOFade(targetFade, 0.25f);


        }
        public static void CloseScreen(GameObject screen, bool isDisableOnFinish = true)
        {
            Image imageBG = screen.transform.GetChild(0).GetComponent<Image>();
            imageBG.transform.SetParent(screen.transform.parent);
            imageBG.transform.localScale = Vector3.one * 75;

            var easeFade = imageBG.DOFade(0f, 0.25f);

            easeFade.onComplete += LocalMethod();


            TweenCallback LocalMethod()
            {
                screen.GetComponent<CanvasGroup>().DOFade(0, 0.5f);
                var ease = screen.transform.DOScale(0.001f, .25f);
                ease.SetEase(Ease.InCubic);
                ease.onComplete += () => { if (isDisableOnFinish) screen.SetActive(false); };
            

                imageBG.DOFade(1f, 0f);
                imageBG.transform.SetParent(screen.transform);
                imageBG.transform.SetAsFirstSibling();

                TweenCallback callback = null;
                return callback;
            }
        }
    }
}