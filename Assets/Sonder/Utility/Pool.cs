﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Matchmania
{
    public class Pool<T> : MonoBehaviour where T : Component
    {
        [Header("Pool properties")]
        [SerializeField] protected T prefab;
        [SerializeField] protected Transform parent;
        [SerializeField] protected int poolAmount = 10;
        [SerializeField] private bool initWhenAwake = true;

        private PoolObjectSpawner<T> spawner;
        protected List<T> objectsList;

        protected virtual void Awake()
        {
            if (initWhenAwake) Init();
        }

        public void Init(T prefab, int poolAmount)
        {
            this.prefab = prefab;
            this.poolAmount = poolAmount;
            Init();
        }

        protected void Init()
        {
            spawner = new PoolObjectSpawner<T>(prefab, parent);
            objectsList = new List<T>(poolAmount);
            for (int i = 0; i < poolAmount; i++)
            {
                IncreasePool(parent.position, parent.rotation);
            }
        }

        private T IncreasePool(Vector3 position, Quaternion rotation)
        {
            var instance = spawner.Spawn(position, rotation);
            objectsList.Add(instance);
            return instance;
        }

        protected T Spawn(Vector3 position, Quaternion rotation)
        {
            foreach (var obj in objectsList)
            {
                if (!obj.gameObject.activeInHierarchy)
                {
                    obj.transform.position = position;
                    obj.transform.rotation = rotation;
                    return obj;
                }
            }
            return IncreasePool(position, rotation);
        }

        protected T Spawn()
        {
            return Spawn(parent.position, parent.rotation);
        }
    }

    public class PoolObjectSpawner<T> where T : Component
    {
        private T prefab;
        private Transform parent;

        public PoolObjectSpawner(T prefab, Transform parent)
        {
            this.prefab = prefab;
            this.parent = parent;
            prefab.gameObject.SetActive(false);
        }

        public T Spawn(Vector3 position, Quaternion rotation)
        {
            return UnityEngine.Object.Instantiate(prefab, position, rotation, parent);
        }
    }
}