using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Matchmania
{
    public class FlyingText : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private TextMeshPool pool;
        [SerializeField] private float flyDistance = 0.25f;
        [SerializeField] private float flyDuration = 1.5f;
        [SerializeField] private float fadeFrom = 0.6f;
        [SerializeField] private float fadeInDuration = 0.1f;
        [SerializeField] private float fadeOutDuration = 0.75f;
        [SerializeField] private Ease ease = Ease.InOutSine;

        public void StartAnimation(Vector3 position, string message)
        {
            var mesh = pool.SpawnTextMesh(position);
            mesh.text = message;

            var startPosition = CanvasPositioningExtensions.WorldToCanvasPosition(canvas, position);
            mesh.GetComponent<RectTransform>().localPosition = startPosition;

            var destination = mesh.rectTransform.localPosition + new Vector3(0.0f, flyDistance, 0.0f);

            var sequence = DOTween.Sequence()
                .Append(mesh.rectTransform.DOLocalMove(destination, flyDuration))
                .Join(mesh.DOFade(1.0f, fadeInDuration).From(fadeFrom))
                .Join(mesh.transform.DOScale(1.0f, fadeInDuration).From(fadeFrom))
                .Insert(flyDuration - fadeOutDuration, mesh.DOFade(0.0f, fadeOutDuration))
                .SetEase(ease)
                .OnComplete(() => mesh.gameObject.SetActive(false))
                .Play();
        }

    }
}
