using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using Random = UnityEngine.Random;
using static System.Net.Mime.MediaTypeNames;

namespace Matchmania
{
    public class CoinsFlyAnimation : MonoBehaviour
    {
       

        public Action OnCompleted;
        public Action OnCompletedSpawn;

        [SerializeField] private MainMenuView MainMenuView;

        [SerializeField] private Camera effectCamera;
        [SerializeField] private RectTransform flyTarget;
        [SerializeField] private RectTransform sparksTarget;
        [SerializeField] private SpritePool coinsPool;
        [SerializeField] private VFXPool glowDotsPool;
        [SerializeField] private Canvas canvas;
        [SerializeField] private SFXConfig SFX_Config;

        [Header("Text view")]
        [SerializeField] private FlyingText flyingText;
        [SerializeField] private float textAppearingTime = 0.25f;

        [Header("Appearing animation")]
        [SerializeField] private float spawnDelay = 0.075f;
        [SerializeField] private float appearDuration = 0.1f;
        [SerializeField] private Ease appearEase = Ease.OutQuad;

        [Header("Tiny jump animation")]
        [SerializeField] private Vector3 tinyOffset = new Vector3(0.0f, -0.1f, 0.0f);
        [SerializeField] private float yRandomness = 0.1f;
        [SerializeField] private float tinyJumpDuration = 0.625f; // it's 0.125f * 5, so at least 5 coins spawns untill fly starts
        [SerializeField] private float tinyJumpPower = 0.4f;

        [Header("Fly animation")]
        [SerializeField] private float flyDuration = 1.5f;
        [SerializeField] private float flyPower = -2.5f;
        [SerializeField] private Ease flyEase = Ease.InQuad;

        [Header("Spawning Area (Rectangle)")]
        public Transform center;
        [SerializeField] private float width = 0.5f;
        [SerializeField] private float height = 0.15f;

        private float OneCoinDuration => tinyJumpDuration + flyDuration;

        public float Duration(int amount) => amount * spawnDelay + OneCoinDuration;

        public void SpawnCoins(int amount, Transform startPosition, RectTransform target = null, int amountText = 0)
        {
            StartCoroutine(SpawnRoutine(amount, startPosition, target, amountText));
        }

        private IEnumerator SpawnRoutine(int amount, Transform startPosition, RectTransform target = null, int amountText = 0)
        {
            // You have to find destination vector. If you use Screen Space - Overlay, uncomment next
            // If you use Screen Space - Camera, just .position  should work well
            //var targetPosition = effectCamera.ScreenToWorldPoint(rectTarget.position);
            var screenPos = CanvasPositioningExtensions.WorldToCanvasPosition(canvas, startPosition.position);
            Vector3 targetPosition;
            if (target == null)
            {
                targetPosition = flyTarget.position;
            }
            else
            {
                targetPosition = target.position;
            }
              
            targetPosition.z = 0;

            if (amountText == 0)
                amountText = amount;

            var textSequence = DOTween.Sequence()
                .InsertCallback(textAppearingTime, () => flyingText.StartAnimation(startPosition.position, amountText.ToString()));

           TimeHeistView.isGivingReward = true;
            for (int i = 0; i < amount; i++)
            {
                var xOffset = Random.Range(0.0f, 1.0f) - 0.5f;
                var yOffset = Random.Range(0.0f, 1.0f) - 0.5f;

           

                var position = new Vector3(screenPos.x + xOffset * width,
                                           screenPos.y + yOffset * height,
                                           screenPos.z);

                

                var coin = coinsPool.Spawn(position);

                coin.rectTransform.localPosition = position;

                var tinyJumpRandom = new Vector3(0.0f, Random.Range(-yRandomness, yRandomness), 0.0f);

                var sequence = coin.rectTransform
                    .DOJump(coin.rectTransform.position + tinyOffset + tinyJumpRandom, tinyJumpPower, 1, tinyJumpDuration)
                    .Join(coin.DOFade(0.0f, appearDuration).From(false).SetEase(appearEase))
                    .Join(coin.rectTransform.DOScale(0.0f, appearDuration).From(false).SetEase(appearEase))
                    .Append(coin.rectTransform.DOJump(targetPosition, flyPower, 1, flyDuration))
                    .OnComplete(() => CompletedAction( coin.gameObject, target))
                    .SetEase(flyEase)
                    .Play();

                // Play gold spawn sound here!
                //AudioPlayer.Instance.PlaySound(SoundEvent.GoldSpawn);
                yield return new WaitForSeconds(spawnDelay);

           
            }
            yield return new WaitForSeconds(1f);
            OnCompletedSpawn?.Invoke();



            // topor, need to fix
            yield return new WaitForSeconds(8);
            OnCompleted = null;

            OnCompletedSpawn = null;

        }

        private void CompletedAction( GameObject coin, RectTransform target = null )
        {
            coin.gameObject.SetActive(false);

            // Use this to
            // 1. Play Sound
            // 2. Increase your view text (I do it manually, only for view, actuall gold number sets in other place)
            // You can do it right here or subscribe to event OnAnimationAlmostCompleted

            // AudioPlayer.Instance.PlaySound(SoundEvent.GoldCollected);
            // coinsView.Increase(1);

            // 3. Play VFX at the coin sprite position. If you use Screen Space - Overlay, you cannot 
            // render VFX in fron of UI. So either considers using Screen Space - Camera (and then just use next lines)
            // or use another approach 
            Vector3 targetPosition;
            if (target == null)
            {
                targetPosition = sparksTarget.position;
            }
            else
            {
                targetPosition = target.position;
            }


            target.DOScale(1.2f, 0.25f).OnComplete(() => { target.DOScale(1, 0.25f); });

            targetPosition.z = center.transform.position.z;
            glowDotsPool.Spawn(targetPosition);

            OnCompleted?.Invoke();


            SFX_Config.PlaySoundEffect(SFX_Config.starHitsUI);
        }

        // Debug line from start to end
        //private void OnDrawGizmosSelected()
        //{
        //    Gizmos.color = Color.green;
        //    var point1 = new Vector3(center.transform.position.x - 0.5f * width, center.transform.position.y + 0.5f * height);
        //    var point2 = new Vector3(center.transform.position.x + 0.5f * width, center.transform.position.y + 0.5f * height);
        //    var point3 = new Vector3(center.transform.position.x + 0.5f * width, center.transform.position.y - 0.5f * height);
        //    var point4 = new Vector3(center.transform.position.x - 0.5f * width, center.transform.position.y - 0.5f * height);
        //    Gizmos.DrawLine(point1, point2);
        //    Gizmos.DrawLine(point2, point3);
        //    Gizmos.DrawLine(point3, point4);
        //    Gizmos.DrawLine(point4, point1);

        //    Gizmos.color = Color.yellow;
        //    var targetPosition = effectCamera.ScreenToWorldPoint(flyTarget.position);
        //    targetPosition.z = center.transform.position.z;
        //    Gizmos.DrawLine(center.transform.position, targetPosition);
        //}
    }
}