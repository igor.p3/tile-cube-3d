using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Matchmania
{
    public class CoinView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI coinsAmount;
        [SerializeField] private CoinsFlyAnimation coinsFlyAnimation;

        private int initialAmount;

        private void Awake()
        {
            coinsFlyAnimation.OnCompleted += OnAnimationAlmostCompleted;
        }

        private void OnDestroy()
        {
            coinsFlyAnimation.OnCompleted -= OnAnimationAlmostCompleted;
        }

        public void OnEnable()
        {
            initialAmount = 10; // Read from Player.Gold data
            coinsAmount.text = $"{initialAmount}";
        }

        public void Increase(int amount)
        {
            initialAmount += amount;
            coinsAmount.text = $"{initialAmount}";
        }

        private void OnAnimationAlmostCompleted()
        {
            Increase(1);
        }
    }
}